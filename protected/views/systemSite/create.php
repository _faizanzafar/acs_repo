<?php
/* @var $this SystemSiteController */
/* @var $model Site */

$this->breadcrumbs=array(
	'Sites'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Site', 'url'=>array('index')),
	array('label'=>'Manage Site', 'url'=>array('admin')),
);
?>

<h1>Create Site</h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'region_List'=>$region_List, 'sub_region_list'=>$sub_region_list, 'zone_list'=>$zone_list)); ?>