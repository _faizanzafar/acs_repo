<?php
/* @var $this SystemSiteController */
/* @var $model Site */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'site-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>30,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'region_id'); ?>
		<?php 
		echo CHtml::activeDropDownList($model, 'region_id', $region_List, array('id'=>'id_region','prompt'=>'Select Region'));
		?>
		<?php echo $form->error($model,'region_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sub_region_id'); ?>
		<?php
		echo CHtml::activeDropDownList($model, 'sub_region_id', $sub_region_list, array('id'=>'id_sub_region','prompt'=>'Select Region First'));
		ECascadeDropDown::master('id_region')->setDependent('id_sub_region',array('dependentLoadingLabel'=>'Loading Sub Regions...'),'SystemUser/SubRegiondata');
		?>
		<?php echo $form->error($model,'sub_region_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'zone_id'); ?>
		<?php
		echo CHtml::activeDropDownList($model, 'zone_id', $zone_list, array('id'=>'id_zone','prompt'=>'Select Sub Region First'));
		ECascadeDropDown::master('id_sub_region')->setDependent('id_zone',array('dependentLoadingLabel'=>'Loading Zones...'),'SystemUser/Zonedata');
		?>
		<?php echo $form->error($model,'zone_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->