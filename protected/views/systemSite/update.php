<?php
/* @var $this SystemSiteController */
/* @var $model Site */

$this->breadcrumbs=array(
	'Sites'=>array('index'),
	$model->name=>array('view','id'=>$model->site_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Site', 'url'=>array('index')),
	array('label'=>'Create Site', 'url'=>array('create')),
	array('label'=>'View Site', 'url'=>array('view', 'id'=>$model->site_id)),
	array('label'=>'Manage Site', 'url'=>array('admin')),
);
?>

<h1>Update Site <?php echo $model->site_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'region_List'=>$region_List, 'sub_region_list'=>$sub_region_list, 'zone_list'=>$zone_list )); ?>