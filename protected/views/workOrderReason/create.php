<?php
/* @var $this WorkOrderReasonController */
/* @var $model WorkOrderReason */

$this->breadcrumbs=array(
	'Work Order Reasons'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List WorkOrderReason', 'url'=>array('index')),
	array('label'=>'Manage WorkOrderReason', 'url'=>array('admin')),
);
?>

<h1>Create WorkOrderReason</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>