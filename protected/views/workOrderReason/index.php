<?php
/* @var $this WorkOrderReasonController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Work Order Reasons',
);

$this->menu=array(
	array('label'=>'Create WorkOrderReason', 'url'=>array('create')),
	array('label'=>'Manage WorkOrderReason', 'url'=>array('admin')),
);
?>

<h1>Work Order Reasons</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
