<?php
/* @var $this WorkOrderReasonController */
/* @var $model WorkOrderReason */

$this->breadcrumbs=array(
	'Work Order Reasons'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List WorkOrderReason', 'url'=>array('index')),
	array('label'=>'Create WorkOrderReason', 'url'=>array('create')),
	array('label'=>'Update WorkOrderReason', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete WorkOrderReason', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage WorkOrderReason', 'url'=>array('admin')),
);
?>

<h1>View WorkOrderReason #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'name',
		'description',
		'created_at',
		'created_by',
		'updated_at',
		'updated_by',
		'id',
	),
)); ?>
