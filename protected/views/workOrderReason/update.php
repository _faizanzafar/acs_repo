<?php
/* @var $this WorkOrderReasonController */
/* @var $model WorkOrderReason */

$this->breadcrumbs=array(
	'Work Order Reasons'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List WorkOrderReason', 'url'=>array('index')),
	array('label'=>'Create WorkOrderReason', 'url'=>array('create')),
	array('label'=>'View WorkOrderReason', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage WorkOrderReason', 'url'=>array('admin')),
);
?>

<h1>Update WorkOrderReason <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>