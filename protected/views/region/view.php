<?php
/* @var $this RegionController */
/* @var $model Region */

$this->breadcrumbs=array(
	'Regions'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Region', 'url'=>array('index')),
	array('label'=>'Create Region', 'url'=>array('create')),
	array('label'=>'Update Region', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Region', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Region', 'url'=>array('admin')),
);
?>

<h1>View Region #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'short_name',
		'country',
		'customer_id',
		'status',
		'created_by',
		'created_at',
	),
)); ?>

<br/>

<?php if( $model->parent == null && sizeof($childs)!=0): ?>
<h3>Child Regions</h3>

<?php foreach($childs as $child) 
{ 
?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$child,
	'attributes'=>array(
		'id',
		'name',
		'short_name',
		'country',
		'customer_id',
		'status',
		'created_by',
		'created_at',
	),
)); ?>

<br/>
<hr/>
<?php } ?>

<?php else:?>

<h3>Parent Region</h3>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$parent,
	'attributes'=>array(
		'id',
		'name',
		'short_name',
		'country',
		'customer_id',
		'status',
		'created_by',
		'created_at',
	),
)); ?>

<?php endif;?>




