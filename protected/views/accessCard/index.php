<?php
/* @var $this AccessCardController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Access Cards',
);

$this->menu=array(
	array('label'=>'Create AccessCard', 'url'=>array('create')),
	array('label'=>'Manage AccessCard', 'url'=>array('admin')),
);
?>

<h1>Access Cards</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
