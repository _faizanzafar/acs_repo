<?php
/* @var $this AccessCardController */
/* @var $model AccessCard */

$this->breadcrumbs=array(
	'Access Cards'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List AccessCard', 'url'=>array('index')),
	array('label'=>'Create AccessCard', 'url'=>array('create')),
	array('label'=>'Update AccessCard', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete AccessCard', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AccessCard', 'url'=>array('admin')),
);
?>

<h1>View AccessCard #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'created_at',
		'created_by',
		'barcode',
		'qr_code',
	),
)); ?>
