<?php
/* @var $this AccessCardController */
/* @var $model AccessCard */

$this->breadcrumbs=array(
	'Access Cards'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AccessCard', 'url'=>array('index')),
	array('label'=>'Create AccessCard', 'url'=>array('create')),
	array('label'=>'View AccessCard', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage AccessCard', 'url'=>array('admin')),
);
?>

<h1>Update AccessCard <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>