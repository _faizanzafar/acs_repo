<?php
/* @var $this AccessCardController */
/* @var $model AccessCard */

$this->breadcrumbs=array(
	'Access Cards'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AccessCard', 'url'=>array('index')),
	array('label'=>'Manage AccessCard', 'url'=>array('admin')),
);
?>

<h1>Create AccessCard</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>