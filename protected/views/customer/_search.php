<?php
/* @var $this CustomerController */
/* @var $model Customer */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'customer_id'); ?>
		<?php echo $form->textField($model,'customer_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'name'); ?>
		<?php echo $form->textField($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'no_sites'); ?>
		<?php echo $form->textField($model,'no_sites'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'head_office_address'); ?>
		<?php echo $form->textField($model,'head_office_address'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'correspondence_office_address'); ?>
		<?php echo $form->textField($model,'correspondence_office_address'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'contact_person_name'); ?>
		<?php echo $form->textField($model,'contact_person_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'contact_person_no'); ?>
		<?php echo $form->textField($model,'contact_person_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'archieve'); ?>
		<?php echo $form->checkBox($model,'archieve'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_at'); ?>
		<?php echo $form->textField($model,'created_at'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->