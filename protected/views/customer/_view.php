<?php
/* @var $this CustomerController */
/* @var $data Customer */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('customer_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->customer_id), array('view', 'id'=>$data->customer_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_sites')); ?>:</b>
	<?php echo CHtml::encode($data->no_sites); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('head_office_address')); ?>:</b>
	<?php echo CHtml::encode($data->head_office_address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('correspondence_office_address')); ?>:</b>
	<?php echo CHtml::encode($data->correspondence_office_address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contact_person_name')); ?>:</b>
	<?php echo CHtml::encode($data->contact_person_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contact_person_no')); ?>:</b>
	<?php echo CHtml::encode($data->contact_person_no); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('archieve')); ?>:</b>
	<?php echo CHtml::encode($data->archieve); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_at')); ?>:</b>
	<?php echo CHtml::encode($data->created_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	*/ ?>

</div>