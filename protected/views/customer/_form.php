<?php
/* @var $this CustomerController */
/* @var $model Customer */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'customer-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name'); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'no_sites'); ?>
		<?php echo $form->textField($model,'no_sites'); ?>
		<?php echo $form->error($model,'no_sites'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'head_office_address'); ?>
		<?php echo $form->textField($model,'head_office_address'); ?>
		<?php echo $form->error($model,'head_office_address'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'correspondence_office_address'); ?>
		<?php echo $form->textField($model,'correspondence_office_address'); ?>
		<?php echo $form->error($model,'correspondence_office_address'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'contact_person_name'); ?>
		<?php echo $form->textField($model,'contact_person_name'); ?>
		<?php echo $form->error($model,'contact_person_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'contact_person_no'); ?>
		<?php echo $form->textField($model,'contact_person_no'); ?>
		<?php echo $form->error($model,'contact_person_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'archieve'); ?>
		<?php echo $form->checkBox($model,'archieve'); ?>
		<?php echo $form->error($model,'archieve'); ?>
	</div>

	<!-- <div class="row">
		<?php echo $form->labelEx($model,'created_at'); ?>
		<?php echo $form->textField($model,'created_at'); ?>
		<?php echo $form->error($model,'created_at'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by'); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div> -->

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->