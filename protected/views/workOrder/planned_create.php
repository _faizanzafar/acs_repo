<?php
/* @var $this WorkOrderController */
/* @var $model WorkOrder */

$this->breadcrumbs=array(
	'Work Orders'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List WorkOrder', 'url'=>array('index')),
	array('label'=>'Manage WorkOrder', 'url'=>array('admin')),
);
?>

<h1>Create Planned WorkOrder</h1>

<?php $this->renderPartial('_planned_form', array('model'=>$model, 'member'=>$member,
 'validatedMembers' => $validatedMembers, 'reason_list'=>$reason_list, 'site_list'=>$site_list,
 'user_list'=>$user_list )); ?>