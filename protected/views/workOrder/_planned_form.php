<?php
/* @var $this WorkOrderController */
/* @var $model WorkOrder */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'work-order-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary(array_merge(array($model),$validatedMembers)); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'reason_id'); ?>

		<?php $this->widget('ext.select2.ESelect2',array(
			  'model'=>$model,
			  'attribute'=>'reason_id',
			  'data'=>$reason_list,
			  'htmlOptions'=>array(
			  	'style'=>'width:200px',   
			    'placeholder' => "Select Reason",
			    // 'onchange'=>'selectSite(this)',
			    // 'onselect'=>'selectSite(this)'
			  ),
			)
		); ?>

		<?php echo $form->error($model,'reason_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'site_id'); ?>

		<?php $this->widget('ext.select2.ESelect2',array(
			  'model'=>$model,
			  'attribute'=>'site_id',
			  'data'=>$site_list,
			  'htmlOptions'=>array(
			  	'style'=>'width:200px',   
			    'placeholder' => "Select Site",
			    // 'onchange'=>'selectSite(this)',
			    // 'onselect'=>'selectSite(this)'
			  ),
			)
		); ?>

		<?php echo $form->error($model,'site_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'work_order_date'); ?>

		<?php Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
	    $this->widget('CJuiDateTimePicker',array(
	    	'model'=>$model, //Model object
	        'attribute'=>'work_order_date', //attribute name
	    	'language'=>'',
	        'mode'=>'date', //use "time","date" or "datetime" (default)
	        'options'=>array(
	        	'regional'=>'',
	        	'timeFormat'=>'hh:mm:ss',
	        	'dateFormat'=>'dd-mm-yy',
	        ) // jquery plugin options
	    ));
		?>

		<?php echo $form->error($model,'work_order_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'start_time'); ?>

		<?php
	    $this->widget('CJuiDateTimePicker',array(
	    	'model'=>$model, //Model object
	        'attribute'=>'start_time', //attribute name
	    	'language'=>'',
	        'mode'=>'time', //use "time","date" or "datetime" (default)
	        'options'=>array(
	        	'regional'=>'',
	        	'timeFormat'=>'hh:mm:ss',
	        	'hourMin'=> 0,
                'hourMax'=> 24,
                'minuteMin'=> 0,
                'minuteMax'=> 60,
                'secondMin'=> 0,
                'secondMax'=> 60,
                'showSecond'=>true,
	        )
	    ));
		?>

		<?php echo $form->error($model,'start_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'end_time'); ?>

		<?php
	    $this->widget('CJuiDateTimePicker',array(
	    	'model'=>$model, //Model object
	        'attribute'=>'end_time', //attribute name
	    	'language'=>'',
	        'mode'=>'time', //use "time","date" or "datetime" (default)
	        'options'=>array(
	        	'regional'=>'',
	        	'timeFormat'=>'hh:mm:ss',
	        	'hourMin'=> 0,
                'hourMax'=> 24,
                'minuteMin'=> 0,
                'minuteMax'=> 60,
                'secondMin'=> 0,
                'secondMax'=> 60,
                'showSecond'=>true,
	        )
	    ));
		?>

		<?php echo $form->error($model,'end_time'); ?>
	</div>


	<h2>Users</h2>

	<?php

    $memberFormConfig = array(
          'elements'=>array(
  			
  			'system_user_name'=>array(
                'type'=>'zii.widgets.jui.CJuiAutoComplete',
                
                'source'=>array_map(function($key, $value) {
				        return array('label' => $value, 'value' => $value);
				    }, array_keys($user_list), $user_list),

                 'options'=>array(
                    'showAnim'=>'fold',
                    'select' =>'js: function(event, ui) {
                        this.value = ui.item.label;
                        return false;
                      }',
                ),
            ),

            'active'=>array(
                'type'=>'dropdownlist',
                'items'=>array(''=>'-',true => 'Active', false => 'Inactive'),
            ),
        ));
 
    $this->widget('ext.multimodelform.MultiModelForm',array(
            'id' => 'id_member', //the unique widget id
            //'tableView' => true,
            'formConfig' => $memberFormConfig, //the form configuration array
            'model' => $member, //instance of the form model
            //'sortAttribute' => 'position', //if assigned: sortable fieldsets is enabled

       		'jsAfterNewId' => MultiModelForm::afterNewIdAutoComplete($memberFormConfig['elements']['system_user_name']),
 
            //if submitted not empty from the controller,
            //the form will be rendered with validation errors
            'validatedItems' => $validatedMembers,
 
            //array of member instances loaded from db
            'data' => $member->findAll('work_order_id=:_id', array(':_id'=>$model->id)),
        ));
    ?>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->