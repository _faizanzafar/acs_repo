<?php
/* @var $this WorkOrderController */
/* @var $model WorkOrder */

$this->breadcrumbs=array(
	'Work Orders'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List WorkOrder', 'url'=>array('index')),
	array('label'=>'Create Planned WorkOrder', 'url'=>array('createPlanned')),
	array('label'=>'Create Unplanned WorkOrder', 'url'=>array('createUnplanned')),
	array('label'=>'Update WorkOrder', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete WorkOrder', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage WorkOrder', 'url'=>array('admin')),
);
?>

<h1>View WorkOrder #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'unique_id',
		'reason_id',
		'site_id',
		'requested_by',
		'planned',
		'work_order_date',
		'start_time',
		'end_time',
		'created_at',
		'created_by',
		'updated_at',
		'updated_by',
	),
)); ?>
