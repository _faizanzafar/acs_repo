<?php
/* @var $this WorkOrderController */
/* @var $model WorkOrder */

$this->breadcrumbs=array(
	'Work Orders'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List WorkOrder', 'url'=>array('index')),
	array('label'=>'Create Planned WorkOrder', 'url'=>array('createPlanned')),
	array('label'=>'Create Unplanned WorkOrder', 'url'=>array('createUnplanned')),
	array('label'=>'View WorkOrder', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage WorkOrder', 'url'=>array('admin')),
);
?>

<h1>Update Unplanned WorkOrder <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_unplanned_form', array('model'=>$model, 'member'=>$member,
 'validatedMembers' => $validatedMembers, 'reason_list'=>$reason_list, 'site_list'=>$site_list )); ?>