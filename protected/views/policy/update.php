<?php
/* @var $this PolicyController */
/* @var $model Policy */

$this->breadcrumbs=array(
	'Policies'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Policy', 'url'=>array('index')),
	array('label'=>'Create Policy', 'url'=>array('create')),
	array('label'=>'View Policy', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'View Policy Assignments', 'url'=>array('policyAssignment', 'id'=>$model->id)),
	array('label'=>'Manage Policy', 'url'=>array('admin')),
);
?>

<h1>Update Policy <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'months_list'=>$months_list, 'days_list'=>$days_list, 'member'=>$member, 'validatedMembers' => $validatedMembers )); ?>