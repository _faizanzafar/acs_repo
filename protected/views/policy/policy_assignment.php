<?php
/* @var $this PolicyAssignmentController */
/* @var $model PolicyAssignment */

$this->breadcrumbs=array(
	'Policy Assignments'=>array('index'),
	$model->id=>array('view','id'=>$model->name),
	'Policy Assignments',
);

$this->menu=array(
	array('label'=>'View Policy', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Update Policy', 'url'=>array('update', 'id'=>$model->id)),
);
?>

<h1>Add "<i><?php echo $model->name; ?></i>" Assignments</h1>



<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'policy-assignment-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary(array_merge(array($model),$validatedMembers)); ?>

	<h2>Users</h2>

	<?php

    $memberFormConfig = array(
          'elements'=>array(
  			
  			'system_user_name'=>array(
                'type'=>'zii.widgets.jui.CJuiAutoComplete',
                
                'source'=>array_map(function($key, $value) {
				        return array('label' => $value, 'value' => $value);
				    }, array_keys($user_list), $user_list),

                 'options'=>array(
                    'showAnim'=>'fold',
                    'select' =>'js: function(event, ui) {
                        this.value = ui.item.label;
                        return false;
                      }',
                ),
            ),

            // 'active'=>array(
            //     'type'=>'dropdownlist',
            //     'items'=>array(''=>'-',true => 'Active', false => 'Inactive'),
            // ),
        ));
 
    $this->widget('ext.multimodelform.MultiModelForm',array(
            'id' => 'id_member', //the unique widget id
            'tableView' => true,
            'formConfig' => $memberFormConfig, //the form configuration array
            'model' => $member, //instance of the form model
            //'sortAttribute' => 'position', //if assigned: sortable fieldsets is enabled

       		'jsAfterNewId' => MultiModelForm::afterNewIdAutoComplete($memberFormConfig['elements']['system_user_name']),
 
            //if submitted not empty from the controller,
            //the form will be rendered with validation errors
            'validatedItems' => $validatedMembers,
 
            //array of member instances loaded from db
            'data' => $member->findAll('policy_id=:_id', array(':_id'=>intval($model->id) )),
        ));
    ?>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->