<?php
/* @var $this PolicyController */
/* @var $model Policy */
/* @var $form CActiveForm */
?>

<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/chosen.min.css">

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'policy-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary(array_merge(array($model),$validatedMembers)); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textField($model,'description',array('size'=>60,'maxlength'=>500)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'start_date_time'); ?>

		<?php Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
	    $this->widget('CJuiDateTimePicker',array(
	    	'model'=>$model, //Model object
	        'attribute'=>'start_date_time', //attribute name
	    	'language'=>'',
	        'mode'=>'datetime', //use "time","date" or "datetime" (default)
	        'options'=>array(
	        	'regional'=>'',
	        	'timeFormat'=>'hh:mm:ss',
	        	'dateFormat'=>'dd-mm-yy',
	        	'hourMin'=> 0,
                'hourMax'=> 24,
                'minuteMin'=> 0,
                'minuteMax'=> 60,
                // 'hourGrid'=>6,
                // 'minuteGrid'=>10,
                'secondMin'=> 0,
                'secondMax'=> 60,
                'showSecond'=>true,
	        ) // jquery plugin options
	    ));
		?>

		<?php echo $form->error($model,'start_date_time'); ?>
	</div>

	

	<div class="row">
		<?php echo $form->labelEx($model,'end_date_time'); ?>

		<?php Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
	    $this->widget('CJuiDateTimePicker',array(
	    	'model'=>$model, //Model object
	        'attribute'=>'end_date_time', //attribute name
	    	'language'=>'',
	        'mode'=>'datetime', //use "time","date" or "datetime" (default)
	        'options'=>array(
	        	'regional'=>'',
	        	'timeFormat'=>'hh:mm:ss',
	        	'dateFormat'=>'dd-mm-yy',
	        	'hourMin'=> 0,
                'hourMax'=> 24,
                'minuteMin'=> 0,
                'minuteMax'=> 60,
                // 'hourGrid'=>6,
                // 'minuteGrid'=>10,
                'secondMin'=> 0,
                'secondMax'=> 60,
                'showSecond'=>true,
	        ) // jquery plugin options
	    ));
		?>

		<?php echo $form->error($model,'end_date_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'selected_months'); ?>
		<?php echo $form->dropDownList($model,'selected_months',$months_list
		, array('id'=>'months_select' ,'multiple' => 'multiple')); 
		//echo $form->textField($model,'role_id'); ?>
		<?php echo $form->error($model,'selected_months'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'selected_week_days'); ?>
		<?php echo $form->dropDownList($model,'selected_week_days',$days_list
		, array('id'=>'weeks_select' ,'multiple' => 'multiple')); 
		//echo $form->textField($model,'role_id'); ?>
		<?php echo $form->error($model,'selected_week_days'); ?>
	</div>

 <h2>Time Slots</h2>

<?php

    $memberFormConfig = array(
          'elements'=>array(

			'start_time'=>array(
                'type'=>'application.extensions.CJuiDateTimePicker.CJuiDateTimePicker',
                'mode'=>'time', //use "time","date" or "datetime" (default)
                'language'=>'en',
                'options'=>array(
                        'regional'=>'',
                        'timeFormat'=>'hh:mm:ss',
		                'hourMin'=> 0,
		                'hourMax'=> 24,
		                'minuteMin'=> 0,
		                'minuteMax'=> 60,
		                // 'hourGrid'=>6,
		                // 'minuteGrid'=>10,
		                'secondMin'=> 0,
		                'secondMax'=> 60,
		                'showSecond'=>true,
                    )
            ),

            'end_time'=>array(
                'type'=>'application.extensions.CJuiDateTimePicker.CJuiDateTimePicker',
                'mode'=>'time', //use "time","date" or "datetime" (default)
                'language'=>'en',
                'options'=>array(
                        'regional'=>'',
                        'timeFormat'=>'hh:mm:ss',
		                'hourMin'=> 0,
		                'hourMax'=> 24,
		                'minuteMin'=> 0,
		                'minuteMax'=> 60,
		                // 'hourGrid'=>6,
		                // 'minuteGrid'=>10,
		                'secondMin'=> 0,
		                'secondMax'=> 60,
		                'showSecond'=>true,
                    )
            ),
        ));
 
    $this->widget('ext.multimodelform.MultiModelForm',array(
            'id' => 'id_member', //the unique widget id
            'tableView' => true,
            'formConfig' => $memberFormConfig, //the form configuration array
            'model' => $member, //instance of the form model

            'jsAfterNewId' => MultiModelForm::afterNewIdDateTimePicker($memberFormConfig['elements']['end_time']),
            'jsAfterNewId' => MultiModelForm::afterNewIdDateTimePicker($memberFormConfig['elements']['start_time']),
 
            //if submitted not empty from the controller,
            //the form will be rendered with validation errors
            'validatedItems' => $validatedMembers,
 
            //array of member instances loaded from db
            'data' => $member->findAll('policy_id=:policyid', array(':policyid'=>$model->id)),
        ));
    ?>


	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->



<script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/chosen.jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
	$('#months_select').chosen({
	no_results_text: "No Month found for",
    width: "95%",
    placeholder_text: 'Select Months'
	});

	$('#weeks_select').chosen({
	no_results_text: "No Day found for",
    width: "95%",
    placeholder_text: 'Select Week Days'
	});
</script>