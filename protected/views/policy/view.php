<?php
/* @var $this PolicyController */
/* @var $model Policy */

$this->breadcrumbs=array(
	'Policies'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Policy', 'url'=>array('index')),
	array('label'=>'Create Policy', 'url'=>array('create')),
	array('label'=>'Update Policy', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Policy Assignments', 'url'=>array('policyAssignment', 'id'=>$model->id)),
	array('label'=>'Delete Policy', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Policy', 'url'=>array('admin')),
);
?>

<h1>View Policy #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'description',
		'start_date_time',
		'end_date_time',
		'created_by',
		'created_at',
		'updated_by',
		'updated_at',
	),
)); ?>

<br/>
<h2>Policy Asignees</h2>

<?php foreach($assignees as $child) 
{
	if($child->system_user_id != null)
		$child->system_user_name = SystemUser::model()->findByPk($child->system_user_id)->full_name;
?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$child,
	'attributes'=>array(
		'id',
		'system_user_name',
		'created_by',
		'created_at',
		'updated_by',
		'updated_at',
	),
)); ?>

<br/>
<hr/>
<?php } ?>
