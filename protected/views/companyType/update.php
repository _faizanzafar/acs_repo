<?php
/* @var $this CompanyTypeController */
/* @var $model CompanyType */

$this->breadcrumbs=array(
	'Company Types'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List CompanyType', 'url'=>array('index')),
	array('label'=>'Create CompanyType', 'url'=>array('create')),
	array('label'=>'View CompanyType', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage CompanyType', 'url'=>array('admin')),
);
?>

<h1>Update CompanyType <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>