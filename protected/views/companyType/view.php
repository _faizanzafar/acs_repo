<?php
/* @var $this CompanyTypeController */
/* @var $model CompanyType */

$this->breadcrumbs=array(
	'Company Types'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List CompanyType', 'url'=>array('index')),
	array('label'=>'Create CompanyType', 'url'=>array('create')),
	array('label'=>'Update CompanyType', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete CompanyType', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CompanyType', 'url'=>array('admin')),
);
?>

<h1>View CompanyType #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'description',
		'created_at',
		'created_by',
	),
)); ?>


<br/>

<?php if( sizeof($companies)!=0): ?>
<h3>Companies</h3>

<?php foreach($companies as $child) 
{ 
?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$child,
	'attributes'=>array(
		'id',
		'name',
		'description',
		'address',
		'company_type_id',
		'created_at',
		'created_by',
		'Administrator',
	),
)); ?>

<br/>
<hr/>
<?php } ?>

<?php endif;?>