<?php
/* @var $this SystemUserController */
/* @var $model SystemUser */

$this->breadcrumbs=array(
	'System Users'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List SystemUser', 'url'=>array('index')),
	array('label'=>'Create SystemUser', 'url'=>array('create')),
	array('label'=>'View SystemUser', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage SystemUser', 'url'=>array('admin')),
);
?>

<h1>Update SystemUser <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model, 
	'member'=>$member, 
	'validatedMembers' => $validatedMembers,
	'availableCardlist'=>$availableCardlist, 
	'customer_list'=>$customer_list, 
	'region_List'=>$region_List, 
	'sub_region_list'=>$sub_region_list, 
	'zone_list'=>$zone_list, 
	'site_list'=>$site_list, 
	'company_type_list'=>$company_type_list, 
	'company_list'=>$company_list 
	)); ?>