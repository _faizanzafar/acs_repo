<?php
/* @var $this SystemUserController */
/* @var $model SystemUser */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'system-user-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
    ),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary(array_merge(array($model),$validatedMembers)); ?>

	<fieldset title="Login Details" >
		<legend>Login Details</legend>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>256)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>256)); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>256)); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'customer_id'); ?>
		<?php echo $form->dropDownList($model,'customer_id',$customer_list, array('empty'=>'Select Customer') );
		//echo $form->textField($model,'customer_id'); ?>

		<?php echo $form->error($model,'customer_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'login'); ?>
		<?php echo $form->checkBox($model,'login'); ?>
		<?php echo $form->error($model,'login'); ?>
	</div>

	</fieldset>

	<fieldset title="Personal Details" >
		<legend>Personal Details</legend>

	<!-- <div class="row">
		<?php echo $form->labelEx($model,'notes'); ?>
		<?php echo $form->textField($model,'notes',array('size'=>60,'maxlength'=>500)); ?>
		<?php echo $form->error($model,'notes'); ?>
	</div> -->

	<div class="row">
		<?php echo $form->labelEx($model,'first_name'); ?>
		<?php echo $form->textField($model,'first_name',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'first_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'last_name'); ?>
		<?php echo $form->textField($model,'last_name',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'last_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'mobile_no'); ?>
		<?php echo $form->textField($model,'mobile_no'); ?>
		<?php echo $form->error($model,'mobile_no'); ?>
	</div>

	<div class="row">
        <?php echo $form->labelEx($model,'image'); ?>
        <?php echo $form->fileField($model, 'image', array("id"=>"img_imput")); ?> 
        <?php echo $form->error($model,'image'); ?>
	</div>

	<div class="row">
	     <?php echo CHtml::image(Yii::app()->request->baseUrl.'/Profile_images/'.$model->profile_image,"",array("width"=>200, "id"=>"img_preview")); ?>  
	</div>

	</fieldset>

	<fieldset title="Access Control Settings" >
		<legend>Access Control Settings</legend>

	<div class="row">
		<?php echo $form->labelEx($model,'company_type_id'); ?>
		<?php 
		echo CHtml::activeDropDownList($model, 'company_type_id', $company_type_list, array('id'=>'id_company_type','prompt'=>'Select Company Type'));
		?>
		<?php echo $form->error($model,'company_type_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'company_id'); ?>
		<?php 
		echo CHtml::activeDropDownList($model, 'company_id', $company_list, array('id'=>'id_company','prompt'=>'Select Company Type First'));
		ECascadeDropDown::master('id_company_type')->setDependent('id_company',array('dependentLoadingLabel'=>'Loading Companies...'),'SystemUser/CompanyData');
		?>
		<?php echo $form->error($model,'company_id'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'region_id'); ?>
		<?php 
		echo CHtml::activeDropDownList($model, 'region_id', $region_List, array('id'=>'id_region','prompt'=>'Select Region'));
		?>
		<?php echo $form->error($model,'region_id'); ?>
		</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sub_region_id'); ?>
		<?php
		echo CHtml::activeDropDownList($model, 'sub_region_id', $sub_region_list, array('id'=>'id_sub_region','prompt'=>'-'));
		ECascadeDropDown::master('id_region')->setDependent('id_sub_region',array('dependentLoadingLabel'=>'Loading Sub Regions...'),'SystemUser/SubRegiondata');
		?>
		<?php echo $form->error($model,'sub_region_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'zone_id'); ?>
		<?php
		echo CHtml::activeDropDownList($model, 'zone_id', $zone_list, array('id'=>'id_zone','prompt'=>'-'));
		ECascadeDropDown::master('id_sub_region')->setDependent('id_zone',array('dependentLoadingLabel'=>'Loading Zones...'),'SystemUser/Zonedata');
		?>
		<?php echo $form->error($model,'zone_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'site_id'); ?>
		<?php
		echo CHtml::activeDropDownList($model, 'site_id', $site_list, array('id'=>'id_site','prompt'=>'-'));
		ECascadeDropDown::master('id_zone')->setDependent('id_site',array('dependentLoadingLabel'=>'Loading Sites...'),'SystemUser/Sitedata');
		?>
		<?php echo $form->error($model,'site_id'); ?>
	</div>



	<!-- <div class="row">
		<?php echo $form->labelEx($model,'mbu_no'); ?>
		<?php echo $form->textField($model,'mbu_no',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'mbu_no'); ?>
	</div> -->

	<h2>Access Cards</h2>

<?php

    $memberFormConfig = array(
          'elements'=>array(

            // 'access_card_id'=>array(
            //     'type'=>'dropdownlist',
            //     'items'=>$availableCardlist,
            //     'width'=>'300px',
            // ),
  			
  			'access_card_id'=>array(
                'type'=>'zii.widgets.jui.CJuiAutoComplete',
                //'source'=>$availableCardlist,
                'source'=>array_map(function($key, $value) {
				        return array('label' => $value, 'value' => $value);
				    }, array_keys($availableCardlist), $availableCardlist),

                 'options'=>array(
                    'showAnim'=>'fold',
                    'select' =>'js: function(event, ui) {
                        this.value = ui.item.label;
                        return false;
                      }',
                ),
            ),

            'active'=>array(
                'type'=>'dropdownlist',
                'items'=>array(''=>'-',true => 'Active', false => 'Inactive'),
            ),
        ));
 
    $this->widget('ext.multimodelform.MultiModelForm',array(
            'id' => 'id_member', //the unique widget id
            //'tableView' => true,
            'formConfig' => $memberFormConfig, //the form configuration array
            'model' => $member, //instance of the form model
            //'sortAttribute' => 'position', //if assigned: sortable fieldsets is enabled

       		'jsAfterNewId' => MultiModelForm::afterNewIdAutoComplete($memberFormConfig['elements']['access_card_id']),
 
            //if submitted not empty from the controller,
            //the form will be rendered with validation errors
            'validatedItems' => $validatedMembers,
 
            //array of member instances loaded from db
            'data' => $member->findAll('system_user_id=:user_id', array(':user_id'=>$model->id)),
        ));
    ?>

	</fieldset>	


	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->


<script type="text/javascript">

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        
        reader.onload = function (e) {
            $('#img_preview').attr('src', e.target.result);
        }
        
        reader.readAsDataURL(input.files[0]);
    }
}

$("#img_imput").change(function(){
    readURL(this);
});

</script>
