<?php
/* @var $this SystemUserController */
/* @var $model SystemUser */

$this->breadcrumbs=array(
	'System Users'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List SystemUser', 'url'=>array('index')),
	array('label'=>'Create SystemUser', 'url'=>array('create')),
	array('label'=>'Update SystemUser', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete SystemUser', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage SystemUser', 'url'=>array('admin')),
);
?>

<h1>View SystemUser #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'email',
		'username',
		'password',
		'last_login_time',
		'created_at',
		'created_by',
		'update_time',
		'update_user_id',
		'customer_id',
		'login',
		'notes',
		'first_name',
		'last_name',
		'region_id',
		'sub_region_id',
		'mbu_no',
		'company_type_id',
		'company_id',
		'mobile_no',
		'profile_image',
		'zone_id',
		'site_id',
	),
)); ?>
