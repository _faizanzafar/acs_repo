<?php

class PolicyController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			// array('allow',  // allow all users to perform 'index' and 'view' actions
			// 	'actions'=>array('index','view'),
			// 	'users'=>array('*'),
			// ),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','view','create','update','admin','delete', 'policyAssignment'),
				'users'=>array('@'),
			),
			// array('allow', // allow admin user to perform 'admin' and 'delete' actions
			// 	'actions'=>array('admin','delete'),
			// 	'users'=>array('admin'),
			// ),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model=Policy::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');

		$assignees = PolicyAssignment::model()->findAll('policy_id=:policy', array(':policy'=>$model->id));

		$this->render('view',array(
			'model'=>$model,
			'assignees'=>$assignees,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		Yii::import('ext.multimodelform.MultiModelForm');

		$model=new Policy;

		$pms_model = new PolicyMonthSetting;
		$pds_model = new PolicyDaySetting;

		$member = new PolicyTimeSlot;

        $validatedMembers = array();
        $validtimeslots = array();

        //default values
        $model->selected_months = array('0'=>'all_months');
        $model->selected_week_days = array('0'=>'all_days');

		if(isset($_POST['Policy']))
		{
			$model->attributes=$_POST['Policy'];

			$pms_model->reset_dropdown_values();
			$pds_model->reset_dropdown_values();

			$second_check = true;
			if(!CClass::validateDate($model->start_date_time))
			{
				$model->addCustomError("start_date_time","Invalid Date Time!");
				$second_check = false;
			}
			if(!CClass::validateDate($model->end_date_time))
			{
				$model->addCustomError("end_date_time","Invalid Date Time!");
				$second_check = false;
			}
			if($second_check && strtotime($model->start_date_time) > strtotime($model->end_date_time) )
			{
				$model->addCustomError("start_date_time","Start Date Time should be earlier then End Date Time!");
			}

			if( sizeof($model->selected_months) > 0 )			
			foreach ($model->selected_months as $key => $value) 
			{
				if( !$pms_model->hasAttribute($value) )
				{
					$model->addCustomError("selected_months","Invalid Values for Month!");
					break;
				}
				else
				{
					$pms_model[$value] = true;
				}
			}

			if( sizeof($model->selected_week_days) > 0 )			
			foreach ($model->selected_week_days as $key => $value) 
			{
				if( !$pds_model->hasAttribute($value) )
				{
					$model->addCustomError("selected_week_days","Invalid Values for Week Days!");
					break;
				}
				else
				{
					$pds_model[$value] = true;
				}
			}
			

			date_default_timezone_set('Asia/Karachi');
			$date = date('Y-m-d H:i:s');

			$model->created_at = $date;
			$model->created_by = Yii::app()->user->id;

			if( MultiModelForm::validate($member,$validatedMembers,$deleteItems) )
			{
				$myCheck = true;
				foreach ($validatedMembers as $key => $value) 
		  		{
		  			$start_time = strtotime($value->start_time);
		  			$end_time = strtotime($value->end_time);

		  			if ( !CClass::test_time($value->start_time) || !CClass::test_time($value->end_time) )
		  			{
		  				$value->addCustomError("start_time","Invalid Time entered!");
						$value->validate();
						$myCheck = false;
		  			}
		  			else if ( $start_time > $end_time )
		  			{		  				
						$value->addCustomError("start_time","Not a valid Time Slot. Start Time should be earlier then End Time.");
						$value->validate();
						$myCheck = false;
		  			}
		  			else if( CClass::time_overlapes($validtimeslots, $start_time, $end_time)  )
		  			{
		  				$value->addCustomError("start_time","This Time Slot is overlapping with other Time Slots.");
		  				$value->validate();
		  				$myCheck = false;
		  			}

		  			$validtimeslots[] = array('start_time'=>$start_time, 'end_time'=>$end_time); 			
		  		}


				if( $myCheck && $model->save())
				{
					$masterValues = array ('policy_id'=>$model->id);
					$pms_model->policy_id = $model->id;
					$pds_model->policy_id = $model->id;


					if( $pds_model->save() && $pms_model->save() && MultiModelForm::save($member,$validatedMembers,$deleteMembers,$masterValues) )
					$this->redirect(array('view','id'=>$model->id));
				}
			}
		}

		$months_list = $pms_model->dropdown_attributes();
		$days_list = $pds_model->dropdown_attributes();

		$this->render('create',array(
			'model'=>$model,

			'months_list'=>$months_list,
			'days_list'=>$days_list,

			'member'=>$member,
            'validatedMembers' => $validatedMembers,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		Yii::import('ext.multimodelform.MultiModelForm');

		$model=$this->loadModel($id);

		$pms_model = PolicyMonthSetting::model()->find('policy_id=:policy', array(':policy'=>$model->id));
		$pds_model = PolicyDaySetting::model()->find('policy_id=:policy', array(':policy'=>$model->id));

		if($pms_model == null)
		{
			$pms_model = new PolicyMonthSetting;
			$model->selected_months = array('0'=>'all_months');
		}
		else
		{
			$model->selected_months = $pms_model->dropdown_selected_values();
		}

		if($pds_model == null)
		{
			$pds_model = new PolicyDaySetting;
			$model->selected_week_days = array('0'=>'all_days');
		}
		else
		{
			$model->selected_week_days = $pds_model->dropdown_selected_values();
		}

		$member = new PolicyTimeSlot;
		$validatedMembers = array();
		$validtimeslots = array();


		if(isset($_POST['Policy']))
		{
			$model->attributes=$_POST['Policy'];

			$pms_model->reset_dropdown_values();
			$pds_model->reset_dropdown_values();

			$second_check = true;
			if(!CClass::validateDate($model->start_date_time))
			{
				$model->addCustomError("start_date_time","Invalid Date Time!");
				$second_check = false;
			}
			if(!CClass::validateDate($model->end_date_time))
			{
				$model->addCustomError("end_date_time","Invalid Date Time!");
				$second_check = false;
			}
			if($second_check && strtotime($model->start_date_time) > strtotime($model->end_date_time) )
			{
				$model->addCustomError("start_date_time","Start Date Time should be earlier then End Date Time!");
			}

			if( sizeof($model->selected_months) > 0 )			
			foreach ($model->selected_months as $key => $value) 
			{
				if( !$pms_model->hasAttribute($value) )
				{
					$model->addCustomError("selected_months","Invalid Values for Month!");
					break;
				}
				else
				{
					$pms_model[$value] = true;
				}
			}

			if( sizeof($model->selected_week_days) > 0 )			
			foreach ($model->selected_week_days as $key => $value) 
			{
				if( !$pds_model->hasAttribute($value) )
				{
					$model->addCustomError("selected_week_days","Invalid Values for Week Days!");
					break;
				}
				else
				{
					$pds_model[$value] = true;
				}
			}

			date_default_timezone_set('Asia/Karachi');
			$date = date('Y-m-d H:i:s');

			$model->updated_at = $date;
			$model->updated_by = Yii::app()->user->id;

			$masterValues = array ('policy_id'=>$model->id);
			$pms_model->policy_id = $model->id;
			$pds_model->policy_id = $model->id;

			MultiModelForm::validate($member,$validatedMembers,$deleteMembers);

			foreach ($validatedMembers as $key => $value) 
	  		{
	  			$start_time = strtotime($value->start_time);
	  			$end_time = strtotime($value->end_time);

	  			if ( !CClass::test_time($value->start_time) || !CClass::test_time($value->end_time) )
	  			{
	  				$value->addCustomError("start_time","Invalid Time entered!");
					$value->validate();
	  			}
	  			else if ( $start_time > $end_time )
	  			{		  				
					$value->addCustomError("start_time","Not a valid Time Slot. Start Time should be earlier then End Time.");
					$value->validate();
	  			}
	  			else if( CClass::time_overlapes($validtimeslots, $start_time, $end_time)  )
	  			{
	  				$value->addCustomError("start_time","This Time Slot is overlapping with other Time Slots.");
	  				$value->validate();
	  			}

	  			$validtimeslots[] = array('start_time'=>$start_time, 'end_time'=>$end_time); 			
	  		}

			if( MultiModelForm::save($member,$validatedMembers,$deleteMembers,$masterValues) && $pms_model->save() 
				&& $pds_model->save() && $model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$model->removeTimeZone();

		$months_list = $pms_model->dropdown_attributes();
		$days_list = $pds_model->dropdown_attributes();

		$this->render('update',array(
			'model'=>$model,

			'months_list'=>$months_list,
			'days_list'=>$days_list,

			'member'=>$member,
            'validatedMembers' => $validatedMembers,
		));
	}

	public function actionPolicyAssignment($id)
	{
		Yii::import('ext.multimodelform.MultiModelForm');

		$model=$this->loadModel($id);

		$member = new PolicyAssignment;

	    $validatedMembers = array();
	    $validusers = array();

		if(isset($_POST['PolicyAssignment']))
		{

			$model->attributes=$_POST['PolicyAssignment'];

			MultiModelForm::validate($member,$validatedMembers,$deleteItems);				

			foreach ($validatedMembers as $key => $value) 
	  		{
	  			$criteria=new CDbCriteria;
	  			$criteria->compare('first_name||\' \'||last_name', ((string)$value->system_user_name) ,true);

	  			$user_selected = SystemUser::model()->find($criteria);

	  			if( $value->system_user_name != '' && $user_selected == null )
				{
					$value->addCustomError("system_user_name","No User with this Name Exists!");
					$value->validate();
				}
	  			else if( in_array($user_selected->id, $validusers) )
	  			{
	  				$value->addCustomError("system_user_name","Cannot add same User Twice!");
	  				$value->validate();
	  			}
	  			else
	  			{
	  				$validusers[] = $user_selected->id;	
	  				$value->system_user_id = $user_selected->id;	  			
	  			}
	  		}

	  		$masterValues = array ('policy_id'=>$model->id );

			if( MultiModelForm::save($member,$validatedMembers,$deleteItems,$masterValues) )
			{
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$policy_list = CHtml::listData(Policy::model()->findAll(), 'id', 'name');
		$user_list = CHtml::listData(SystemUser::model()->findAll(), 'id', 'full_name');

		$this->render('policy_assignment',array(
			'model'=>$model,
			'policy_list'=>$policy_list,
			'user_list'=>$user_list,

			'member'=>$member,
            'validatedMembers' => $validatedMembers,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Policy');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Policy('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Policy']))
			$model->attributes=$_GET['Policy'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Policy the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Policy::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Policy $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='policy-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
