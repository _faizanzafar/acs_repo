<?php

class SystemSiteController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			// array('allow',  // allow all users to perform 'index' and 'view' actions
			// 	'actions'=>array('index','view'),
			// 	'users'=>array('*'),
			// ),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','view','create','update','admin','delete','SubRegiondata','Zonedata'),
				'users'=>array('@'),
			),
			// array('allow', // allow admin user to perform 'admin' and 'delete' actions
			// 	'actions'=>array('admin','delete'),
			// 	'users'=>array('admin'),
			// ),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Site;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Site']))
		{
			$model->attributes=$_POST['Site'];

			date_default_timezone_set('Asia/Karachi');
			$date = date('Y-m-d H:i:s');

			$model->created_at = $date;
			$model->created_by = Yii::app()->user->id;

			if($model->save())
				$this->redirect(array('view','id'=>$model->site_id));
		}

		$criteria=new CDbCriteria;
  		$criteria->select="t.id, t.name"; 
  		$criteria->condition="t.parent IS NULL";
		$region_List = CHtml::listData(Region::model()->findAll($criteria), 'id', 'name');

		$sub_region_list = CHtml::listData(Region::model()->findAll('parent=:parent_id', array(':parent_id'=>intval($model->region_id) )), 'id', 'name');

		$zone_list = CHtml::listData(Region::model()->findAll('parent=:parent_id', array(':parent_id'=>intval($model->sub_region_id) )), 'id', 'name');

		$this->render('create',array(
			'model'=>$model,
			'region_List'=>$region_List,
			'sub_region_list'=>$sub_region_list,
			'zone_list'=>$zone_list,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Site']))
		{
			$model->attributes=$_POST['Site'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->site_id));
		}

		$criteria=new CDbCriteria;
  		$criteria->select="t.id, t.name"; 
  		$criteria->condition="t.parent IS NULL";
		$region_List = CHtml::listData(Region::model()->findAll($criteria), 'id', 'name');

		$sub_region_list = CHtml::listData(Region::model()->findAll('parent=:parent_id', array(':parent_id'=>intval($model->region_id) )), 'id', 'name');

		$zone_list = CHtml::listData(Region::model()->findAll('parent=:parent_id', array(':parent_id'=>intval($model->sub_region_id) )), 'id', 'name');

		$this->render('update',array(
			'model'=>$model,
			'region_List'=>$region_List,
			'sub_region_list'=>$sub_region_list,
			'zone_list'=>$zone_list,
		));
	}

	public function actionSubRegiondata()
	{
		$chk_val = null;
	   	ECascadeDropDown::checkValidRequest();

	   	if(is_numeric(ECascadeDropDown::submittedKeyValue()))
	   		$chk_val = ECascadeDropDown::submittedKeyValue();

	   	$data = Region::model()->findAll('parent=:parent_id', array(':parent_id'=>$chk_val));

	   	if( sizeof($data) > 0 )
	   		ECascadeDropDown::renderListData($data,'id', 'name');
	   	else if($chk_val == null)
	   		ECascadeDropDown::renderEmptyData('Select Region First');
	   	else
	   		ECascadeDropDown::renderEmptyData('No Data Found!');
	}

	public function actionZonedata()
	{
		$chk_val = null;
	   	ECascadeDropDown::checkValidRequest();

	   	if(is_numeric(ECascadeDropDown::submittedKeyValue()))
	   		$chk_val = ECascadeDropDown::submittedKeyValue();

	   	$data = Region::model()->findAll('parent=:parent_id', array(':parent_id'=>$chk_val));

	   	if( sizeof($data) > 0 )
	   		ECascadeDropDown::renderListData($data,'id', 'name');
	   	else if($chk_val == null)
	   		ECascadeDropDown::renderEmptyData('Select Sub Region First');
	   	else
	   		ECascadeDropDown::renderEmptyData('No Data Found!');
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Site');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Site('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Site']))
			$model->attributes=$_GET['Site'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Site the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Site::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Site $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='site-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
