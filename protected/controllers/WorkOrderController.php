<?php

class WorkOrderController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			// array('allow',  // allow all users to perform 'index' and 'view' actions
			// 	'actions'=>array('index','view'),
			// 	'users'=>array('*'),
			// ),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','view','create','update','admin','delete'),
				'users'=>array('@'),
			),
			// array('allow', // allow admin user to perform 'admin' and 'delete' actions
			// 	'actions'=>array('admin','delete'),
			// 	'users'=>array('admin'),
			// ),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreatePlanned()
	{
		Yii::import('ext.multimodelform.MultiModelForm');

		$model = new WorkOrder;
		$member = new WorkOrderPlannedUserAssignment;

        $validatedMembers = array();
        $validusers = array();

        $model->start_time = "00:00:00";
        $model->end_time = "23:59:59";

		if(isset($_POST['WorkOrder']))
		{
			$model->attributes=$_POST['WorkOrder'];

			date_default_timezone_set('Asia/Karachi');
			$date = date('Y-m-d H:i:s');

			$model->created_at = $date;
			$model->created_by = Yii::app()->user->id;
			$model->unique_id = uniqid("",true);
			$model->requested_by = Yii::app()->user->id;
			$model->planned = true;

			if(!CClass::validateDate($model->start_time,'H:i:s'))
			{
				$model->addCustomError("start_time","Invalid Time Entered!");
			}
			if(!CClass::validateDate($model->end_time,'H:i:s'))
			{
				$model->addCustomError("end_time","Invalid Time Entered!");
			}
			if($model->work_order_date!="" && !CClass::validateDate($model->work_order_date,'d-m-Y'))
			{
				$model->addCustomError("work_order_date","Invalid Date Entered!");
			}

			if(MultiModelForm::validate($member,$validatedMembers,$deleteItems))
			{
				$myCheck = true;
				foreach ($validatedMembers as $key => $value) 
		  		{
		  			$criteria=new CDbCriteria;
		  			$criteria->compare('first_name||\' \'||last_name', ((string)$value->system_user_name) ,true);

		  			$user_selected = SystemUser::model()->find($criteria);

		  			if( $value->system_user_name != '' && $user_selected == null )
					{
						$value->addCustomError("system_user_name","No User with this Name Exists!");
						$value->validate();
						$myCheck = false;
					}
		  			else if( in_array($user_selected->id, $validusers) )
		  			{
		  				$value->addCustomError("system_user_name","Cannot add same User Twice!");
		  				$value->validate();
		  				$myCheck = false;
		  			}
		  			else
		  			{
		  				$validusers[] = $user_selected->id;
		  				$value->system_user_id = $user_selected->id;
		  			}
		  		}

				if( $myCheck && $model->save() )
				{
					$masterValues = array ('work_order_id'=>$model->id);

	                if (MultiModelForm::save($member,$validatedMembers,$deleteItems,$masterValues))
						$this->redirect(array('view','id'=>$model->id));
				}
			}
		}

		$reason_list =  CHtml::listData(WorkOrderReason::model()->findAll(), 'id', 'name');
		$site_list =  CHtml::listData(Site::model()->findAll(), 'site_id', 'name');

		$user_list = CHtml::listData(SystemUser::model()->findAll(), 'id', 'full_name');
		$user_list [''] = '-';

		foreach ($validatedMembers as $key => $value) 
	  	{
	  		$value->active = ($value->active == true) ? 1 : 0;	
	  	}

		$this->render('planned_create',array(
			'model'=>$model,

			'member'=>$member,
            'validatedMembers' => $validatedMembers,

			'reason_list'=>$reason_list,
			'site_list'=>$site_list,
			'user_list'=>$user_list,
		));
	}

	public function actionCreateUnplanned()
	{
		Yii::import('ext.multimodelform.MultiModelForm');

		$model = new WorkOrder;
		$member = new WorkOrderUnplannedUser;

        $validatedMembers = array();
        $valid_identifications = array();

        $model->start_time = "00:00:00";
        $model->end_time = "23:59:59";

		if(isset($_POST['WorkOrder']))
		{
			$model->attributes=$_POST['WorkOrder'];

			date_default_timezone_set('Asia/Karachi');
			$date = date('Y-m-d H:i:s');

			$model->created_at = $date;
			$model->created_by = Yii::app()->user->id;
			$model->unique_id = uniqid("",true);
			$model->requested_by = Yii::app()->user->id;
			$model->planned = false;

			if(!CClass::validateDate($model->start_time,'H:i:s'))
			{
				$model->addCustomError("start_time","Invalid Time Entered!");
			}
			if(!CClass::validateDate($model->end_time,'H:i:s'))
			{
				$model->addCustomError("end_time","Invalid Time Entered!");
			}
			if($model->work_order_date!="" && !CClass::validateDate($model->work_order_date,'d-m-Y'))
			{
				$model->addCustomError("work_order_date","Invalid Date Entered!");
			}
			
			if(MultiModelForm::validate($member,$validatedMembers,$deleteItems) )
			{
				$myCheck = true;
				foreach ($validatedMembers as $key => $value) 
		  		{
		  			if( in_array($value->identification_no, $valid_identifications) )
		  			{
		  				$value->addCustomError("identification_no","Cannot add User with same Identification number!");
		  				$value->validate();
		  				$myCheck = false;
		  			}
		  			else
		  				$valid_identifications[] = $value->identification_no;		  			
		  		}

				if( $myCheck && $model->save() )
				{
					$masterValues = array ('work_order_id'=>$model->id);

	                if (MultiModelForm::save($member,$validatedMembers,$deleteItems,$masterValues))
						$this->redirect(array('view','id'=>$model->id));
				}
			}
		}

		$reason_list =  CHtml::listData(WorkOrderReason::model()->findAll(), 'id', 'name');
		$site_list =  CHtml::listData(Site::model()->findAll(), 'site_id', 'name');


		$this->render('unplanned_create',array(
			'model'=>$model,

			'member'=>$member,
            'validatedMembers' => $validatedMembers,

			'reason_list'=>$reason_list,
			'site_list'=>$site_list,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		Yii::import('ext.multimodelform.MultiModelForm');

		$model=$this->loadModel($id);

		if($model->planned)
		{
			$member = new WorkOrderPlannedUserAssignment;

		    $validatedMembers = array();
		    $validusers = array();

			if(isset($_POST['WorkOrder']))
			{
				$model->attributes=$_POST['WorkOrder'];

				date_default_timezone_set('Asia/Karachi');
				$date = date('Y-m-d H:i:s');

				$model->updated_at = $date;
				$model->updated_by = Yii::app()->user->id;

				if(!CClass::validateDate($model->start_time,'H:i:s'))
				{
					$model->addCustomError("start_time","Invalid Time Entered!");
				}
				if(!CClass::validateDate($model->end_time,'H:i:s'))
				{
					$model->addCustomError("end_time","Invalid Time Entered!");
				}
				if($model->work_order_date!="" && !CClass::validateDate($model->work_order_date,'d-m-Y'))
				{
					$model->addCustomError("work_order_date","Invalid Date Entered!");
				}

				MultiModelForm::validate($member,$validatedMembers,$deleteItems);				

				foreach ($validatedMembers as $key => $value) 
		  		{
		  			$criteria=new CDbCriteria;
		  			$criteria->compare('first_name||\' \'||last_name', ((string)$value->system_user_name) ,true);

		  			$user_selected = SystemUser::model()->find($criteria);

		  			if( $value->system_user_name != '' && $user_selected == null )
					{
						$value->addCustomError("system_user_name","No User with this Name Exists!");
						$value->validate();
					}
		  			else if( in_array($user_selected->id, $validusers) )
		  			{
		  				$value->addCustomError("system_user_name","Cannot add same User Twice!");
		  				$value->validate();
		  			}
		  			else
		  			{
		  				$validusers[] = $user_selected->id;	
		  				$value->system_user_id = $user_selected->id;	  			
		  			}
		  		}

		  		$masterValues = array ('work_order_id'=>$model->id);

				if( MultiModelForm::save($member,$validatedMembers,$deleteItems,$masterValues) && $model->save() )
				{
					$this->redirect(array('view','id'=>$model->id));
				}				
			}

			$reason_list =  CHtml::listData(WorkOrderReason::model()->findAll(), 'id', 'name');
			$site_list =  CHtml::listData(Site::model()->findAll(), 'site_id', 'name');

			$user_list = CHtml::listData(SystemUser::model()->findAll(), 'id', 'full_name');
			$user_list [''] = '-';

			foreach ($validatedMembers as $key => $value) 
		  	{
		  		$value->active = ($value->active == true) ? 1 : 0;	
		  	}

			$this->render('planned_update',array(
				'model'=>$model,

				'member'=>$member,
		        'validatedMembers' => $validatedMembers,

				'reason_list'=>$reason_list,
				'site_list'=>$site_list,
				'user_list'=>$user_list,
			));
		}
		else
		{
			$member = new WorkOrderUnplannedUser;

	        $validatedMembers = array();
	        $valid_identifications = array();

			if(isset($_POST['WorkOrder']))
			{
				$model->attributes=$_POST['WorkOrder'];

				date_default_timezone_set('Asia/Karachi');
				$date = date('Y-m-d H:i:s');

				$model->updated_at = $date;
				$model->updated_by = Yii::app()->user->id;

				if(!CClass::validateDate($model->start_time,'H:i:s'))
				{
					$model->addCustomError("start_time","Invalid Time Entered!");
				}
				if(!CClass::validateDate($model->end_time,'H:i:s'))
				{
					$model->addCustomError("end_time","Invalid Time Entered!");
				}
				if($model->work_order_date!="" && !CClass::validateDate($model->work_order_date,'d-m-Y'))
				{
					$model->addCustomError("work_order_date","Invalid Date Entered!");
				}
				
				if(MultiModelForm::validate($member,$validatedMembers,$deleteItems) )
				{
					foreach ($validatedMembers as $key => $value) 
			  		{
			  			if( in_array($value->identification_no, $valid_identifications) )
			  			{
			  				$value->addCustomError("identification_no","Cannot add User with same Identification number!");
			  				$value->validate();
			  			}
			  			else
			  				$valid_identifications[] = $value->identification_no;		  			
			  		}

			  		$masterValues = array ('work_order_id'=>$model->id);

					if( MultiModelForm::save($member,$validatedMembers,$deleteItems,$masterValues) && $model->save() )
					{
						$this->redirect(array('view','id'=>$model->id));
					}
				}
			}

			$reason_list =  CHtml::listData(WorkOrderReason::model()->findAll(), 'id', 'name');
			$site_list =  CHtml::listData(Site::model()->findAll(), 'site_id', 'name');


			$this->render('unplanned_create',array(
				'model'=>$model,

				'member'=>$member,
	            'validatedMembers' => $validatedMembers,

				'reason_list'=>$reason_list,
				'site_list'=>$site_list,
			));
		}
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('WorkOrder');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new WorkOrder('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['WorkOrder']))
			$model->attributes=$_GET['WorkOrder'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return WorkOrder the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=WorkOrder::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param WorkOrder $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='work-order-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
