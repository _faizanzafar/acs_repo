<?php

class SystemUserController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			// array('allow',  // allow all users to perform 'index' and 'view' actions
			// 	'actions'=>array('index','view'),
			// 	'users'=>array('*'),
			// ),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','view','create','update','admin','delete','SubRegiondata','CompanyData','Zonedata','Sitedata'),
				'users'=>array('@'),
			),
			// array('allow', // allow admin user to perform 'admin' and 'delete' actions
			// 	'actions'=>array('admin','delete'),
			// 	'users'=>array('admin'),
			// ),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		Yii::import('ext.multimodelform.MultiModelForm');

		$model=new SystemUser;
        $member = new SystemUserHasAccessCard;

        $validatedMembers = array();
        $validAccessCardID = array();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SystemUser']))
		{
			$model->attributes=$_POST['SystemUser'];

			date_default_timezone_set('Asia/Karachi');
			$date = date('Y-m-d H:i:s');

			$model->created_at = $date;
			$model->created_by = Yii::app()->user->id;

			$uploadedFile = CUploadedFile::getInstance($model,'image');

			if(!empty($uploadedFile)){
				$fileName = "{$model->username}".time()."-ProfileImage." . $uploadedFile->getExtensionName();
            	$model->profile_image = $fileName;
			}

			

			if(MultiModelForm::validate($member,$validatedMembers,$deleteItems) )
			{
				$myCheck = true;
				foreach ($validatedMembers as $key => $value) 
		  		{
		  			$validcard = AccessCard::model()->
		  			findByAttributes( array('qr_code'=> ((string)$value->access_card_id) ));

		  			$alreadyCard = SystemUserHasAccessCard::model()->
					findAll('access_card_id=:curr_card AND active IS TRUE',
					array(':curr_card'=>intval($value->switchBackId()) ));

		  			if($value->access_card_id != '' && $validcard == null )
					{
						$value->addCustomError("access_card_id","No Card with this QR code Exists!");
						$value->validate();
						$myCheck = false;
					}
	  				else if ( sizeof($alreadyCard) != 0 )
		  			{		  				
						$value->addCustomError("access_card_id","This Card is Assigned to another User!");
						$value->validate();
						$myCheck = false;
		  			}
		  			else if( in_array($value->access_card_id, $validAccessCardID) )
		  			{
		  				$value->addCustomError("access_card_id","Cannot Add Same Access Card Twice!");
		  				$value->validate();
		  				$myCheck = false;
		  			}
		  			else
		  				$validAccessCardID[] = $value->access_card_id;		  			
		  		}

		  		if($myCheck && $model->save())
		  		{
		  			if (!file_exists(Yii::app()->basePath.'/../profile_images')) {
				    	mkdir(Yii::app()->basePath.'/../profile_images', 0777, true);
					}

					if(!empty($uploadedFile))
					{
						$uploadedFile->saveAs(Yii::app()->basePath.'/../profile_images/'.$fileName);
					}

	                 $masterValues = array ('system_user_id'=>$model->id,
	                 						'created_by'=>Yii::app()->user->id, 
	                 						'created_at'=>$date );

	                 if (MultiModelForm::save($member,$validatedMembers,$deleteItems,$masterValues))
	                 	$this->redirect(array('view','id'=>$model->id));
		  		}
			}
		}



		$criteria=new CDbCriteria;
  		$criteria->select="t.id, t.name"; 
  		$criteria->condition="t.parent IS NULL";
		$region_List = CHtml::listData(Region::model()->findAll($criteria), 'id', 'name');

		$sub_region_list = CHtml::listData(Region::model()->findAll('parent=:parent_id', array(':parent_id'=>intval($model->region_id) )), 'id', 'name');

		$zone_list = CHtml::listData(Region::model()->findAll('parent=:parent_id', array(':parent_id'=>intval($model->sub_region_id) )), 'id', 'name');

		$site_list = CHtml::listData(Site::model()->findAll('region_id=:region_id AND sub_region_id=:sub_region_id AND zone_id=:zone_id', array(
			':region_id'=>intval($model->region_id),
			':sub_region_id'=>intval($model->sub_region_id),
			':zone_id'=>intval($model->zone_id)
		)), 'site_id', 'name');


		$company_type_list =  CHtml::listData(CompanyType::model()->findAll(), 'id', 'name');
		$company_list = CHtml::listData(Company::model()->findAll('company_type_id=:parent_id', array(':parent_id'=>intval($model->company_type_id) )), 'id', 'name');

		$customer_list = CHtml::listData(customer::model()->findAll(), 'customer_id', 'name');

		$criteria=new CDbCriteria;
  		$criteria->select="t.id, t.access_card_id"; 
  		$criteria->condition="t.active IS TRUE";
		$cr = new CDbCriteria();
		$cr->addNotInCondition('id', CHtml::listData( SystemUserHasAccessCard::model()->findAll($criteria),"id","access_card_id" ) );

		
		$availableCardlist = CHtml::listData(AccessCard::model()->findAll($cr), 'id', 'qr_code');
		$availableCardlist [''] = '-';

		asort($availableCardlist);

		foreach ($validatedMembers as $key => $value) 
	  	{
	  		$value->access_card_id = $value->switchToString();
	  		$value->active = ($value->active == true) ? 1 : 0;	
	  	}

		$this->render('create',array(
			'model'=>$model,

			'member'=>$member,
            'validatedMembers' => $validatedMembers,

            'availableCardlist'=>$availableCardlist,
			'customer_list'=>$customer_list,

			'region_List'=>$region_List,
			'sub_region_list'=>$sub_region_list,
			'zone_list'=>$zone_list,
			'site_list'=>$site_list,

			'company_type_list'=>$company_type_list,
			'company_list'=>$company_list,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{

		Yii::import('ext.multimodelform.MultiModelForm');

		$model=$this->loadModel($id);
        $member = new SystemUserHasAccessCard;
        $validatedMembers = array();
        $validAccessCardID = array();

		if(isset($_POST['SystemUser']))
		{
			$uploadedFile=CUploadedFile::getInstance($model,'image');

			if( $model->profile_image == null && !empty($uploadedFile) )
			{
				$model->profile_image = "{$model->username}".time()."-ProfileImage." . $uploadedFile->getExtensionName();
			}

			$_POST['SystemUser']['profile_image'] = $model->profile_image;
            $model->attributes=$_POST['SystemUser'];

			date_default_timezone_set('Asia/Karachi');
			$date = date('Y-m-d H:i:s');

			$model->update_time = $date;
			$model->update_user_id = Yii::app()->user->id;

		  	$masterValues = array ('system_user_id'=>$model->id,
                 						'created_by'=>Yii::app()->user->id, 
                 						'created_at'=>$date );

            MultiModelForm::validate($member,$validatedMembers,$deleteMembers);
		  	
	  		foreach ($validatedMembers as $key => $value) 
	  		{
	  			$validcard = AccessCard::model()->
	  			findByAttributes( array('qr_code'=> ((string)$value->access_card_id) ));
	  			
	  			$alreadyCard = SystemUserHasAccessCard::model()->
				findAll('access_card_id=:curr_card AND system_user_id!=:curr_user AND active IS TRUE',
				array(':curr_card'=>intval($value->switchBackId()) , ':curr_user'=> intval($model->id) ));

				if($value->access_card_id != '' && $validcard == null )
				{
					$value->addCustomError("access_card_id","No Card with this QR code Exists!");
				}
	  			else if ( sizeof($alreadyCard) != 0 )
	  			{		  				
					$value->addCustomError("access_card_id","This Card is Assigned to another User!");
	  			}
	  			else if( in_array($value->access_card_id, $validAccessCardID) )
	  			{
	  				$value->addCustomError("access_card_id","Cannot Add Same Access Card Twice!");
	  			}
	  			else
	  				$validAccessCardID[] = $value->access_card_id;	

	  			$value->access_card_id = $value->switchToString();		
	  		}



			if(MultiModelForm::save($member,$validatedMembers,$deleteMembers,$masterValues) && $model->save())
			{
				if (!file_exists(Yii::app()->basePath.'/../profile_images')) {
				    mkdir(Yii::app()->basePath.'/../profile_images', 0777, true);
				}

				if(!empty($uploadedFile))
                {
                    $uploadedFile->saveAs(Yii::app()->basePath.'/../Profile_images/'.$model->profile_image);
                }
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$criteria=new CDbCriteria;
  		$criteria->select="t.id, t.name"; 
  		$criteria->condition="t.parent IS NULL";
		$region_List = CHtml::listData(Region::model()->findAll($criteria), 'id', 'name');

		$sub_region_list = CHtml::listData(Region::model()->findAll('parent=:parent_id', array(':parent_id'=>intval($model->region_id) )), 'id', 'name');

		$zone_list = CHtml::listData(Region::model()->findAll('parent=:parent_id', array(':parent_id'=>intval($model->sub_region_id) )), 'id', 'name');

		$site_list = CHtml::listData(Site::model()->findAll('region_id=:region_id AND sub_region_id=:sub_region_id AND zone_id=:zone_id', array(
			':region_id'=>intval($model->region_id),
			':sub_region_id'=>intval($model->sub_region_id),
			':zone_id'=>intval($model->zone_id)
		)), 'site_id', 'name');


		$company_type_list =  CHtml::listData(CompanyType::model()->findAll(), 'id', 'name');

		$company_list = CHtml::listData(Company::model()->findAll('company_type_id=:parent_id', array(':parent_id'=>intval($model->company_type_id) )), 'id', 'name');

		$customer_list = CHtml::listData(customer::model()->findAll(), 'customer_id', 'name');		

		$cr = new CDbCriteria();

		$criteria=new CDbCriteria;
  		$criteria->select="t.id, t.access_card_id"; 
  		$criteria->condition="t.active IS TRUE AND t.system_user_id!=:user_id";
  		$criteria->params=array( ':user_id'=>$model->id );

		$cr->addNotInCondition('id', CHtml::listData( SystemUserHasAccessCard::model()->findAll($criteria),"id","access_card_id" ) );

		$availableCardlist = CHtml::listData(AccessCard::model()->findAll($cr), 'id', 'qr_code');
		$availableCardlist [''] = '-';

		asort($availableCardlist);

		foreach ($validatedMembers as $key => $value) 
	  	{
	  		$value->access_card_id = $value->switchToString();
	  		$value->active = ($value->active == true) ? 1 : 0;	
	  	}

		$this->render('update',array(
			'model'=>$model,

			'member'=>$member,
            'validatedMembers' => $validatedMembers,

            'availableCardlist'=>$availableCardlist,
			'customer_list'=>$customer_list,

			'region_List'=>$region_List,
			'sub_region_list'=>$sub_region_list,
			'zone_list'=>$zone_list,
			'site_list'=>$site_list,

			'company_type_list'=>$company_type_list,
			'company_list'=>$company_list,
		));
	}

	public function actionSubRegiondata()
	{
		$chk_val = null;
	   	ECascadeDropDown::checkValidRequest();

	   	if(is_numeric(ECascadeDropDown::submittedKeyValue()))
	   		$chk_val = ECascadeDropDown::submittedKeyValue();

	   	$data = Region::model()->findAll('parent=:parent_id', array(':parent_id'=>$chk_val));

	   	if( sizeof($data) > 0 )
	   		ECascadeDropDown::renderListData($data,'id', 'name');
	   	else if($chk_val == null)
	   		ECascadeDropDown::renderEmptyData('Select Region First');
	   	else
	   		ECascadeDropDown::renderEmptyData('No Data Found!');
	}

	public function actionZonedata()
	{
		$chk_val = null;
	   	ECascadeDropDown::checkValidRequest();

	   	if(is_numeric(ECascadeDropDown::submittedKeyValue()))
	   		$chk_val = ECascadeDropDown::submittedKeyValue();

	   	$data = Region::model()->findAll('parent=:parent_id', array(':parent_id'=>$chk_val));

	   	if( sizeof($data) > 0 )
	   		ECascadeDropDown::renderListData($data,'id', 'name');
	   	else if($chk_val == null)
	   		ECascadeDropDown::renderEmptyData('Select Sub Region First');
	   	else
	   		ECascadeDropDown::renderEmptyData('No Data Found!');
	}

	public function actionSitedata()
	{
		$chk_val = null;
	   	ECascadeDropDown::checkValidRequest();

	   	if(is_numeric(ECascadeDropDown::submittedKeyValue()))
	   		$chk_val = ECascadeDropDown::submittedKeyValue();

	   	$data = Site::model()->findAll('zone_id=:parent_id', array(':parent_id'=>$chk_val));

	   	if( sizeof($data) > 0 )
	   		ECascadeDropDown::renderListData($data,'id', 'name');
	   	else if($chk_val == null)
	   		ECascadeDropDown::renderEmptyData('Select Zone First');
	   	else
	   		ECascadeDropDown::renderEmptyData('No Data Found!');
	}

	public function actionCompanyData()
	{
		$chk_val = null;
	   	ECascadeDropDown::checkValidRequest();

	   	if(is_numeric(ECascadeDropDown::submittedKeyValue()))
	   		$chk_val = ECascadeDropDown::submittedKeyValue();

	    $data = Company::model()->findAll('company_type_id=:parent_id', array(':parent_id'=>$chk_val));

	   	if( sizeof($data) > 0 )
	   		ECascadeDropDown::renderListData($data,'id', 'name');
	   	else if($chk_val == null)
	   		ECascadeDropDown::renderEmptyData('Select Company Type First');
	   	else
	   		ECascadeDropDown::renderEmptyData('No Data Found!');
	}


	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('SystemUser');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new SystemUser('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SystemUser']))
			$model->attributes=$_GET['SystemUser'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return SystemUser the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=SystemUser::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param SystemUser $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='system-user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
