<?php

/**
 * This is the model class for table "policy_day_setting".
 *
 * The followings are the available columns in table 'policy_day_setting':
 * @property integer $id
 * @property integer $policy_id
 * @property boolean $all_days
 * @property boolean $sunday
 * @property boolean $monday
 * @property boolean $teusday
 * @property boolean $wednesday
 * @property boolean $thursday
 * @property boolean $friday
 * @property boolean $saturday
 *
 * The followings are the available model relations:
 * @property Policy $policy
 */
class PolicyDaySetting extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'policy_day_setting';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('policy_id', 'required'),
			array('policy_id', 'numerical', 'integerOnly'=>true),
			array('all_days, sunday, monday, teusday, wednesday, thursday, friday, saturday', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, policy_id, all_days, sunday, monday, teusday, wednesday, thursday, friday, saturday', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'policy' => array(self::BELONGS_TO, 'Policy', 'policy_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'policy_id' => 'Policy',
			'all_days' => 'All Days',
			'sunday' => 'Sunday',
			'monday' => 'Monday',
			'teusday' => 'Teusday',
			'wednesday' => 'Wednesday',
			'thursday' => 'Thursday',
			'friday' => 'Friday',
			'saturday' => 'Saturday',
		);
	}

	public function dropdown_attributes ()
	{
		return array(
			'all_days' => 'All Days',
			'sunday' => 'Sunday',
			'monday' => 'Monday',
			'teusday' => 'Teusday',
			'wednesday' => 'Wednesday',
			'thursday' => 'Thursday',
			'friday' => 'Friday',
			'saturday' => 'Saturday',
		);
	}

	public function reset_dropdown_values()
	{
		foreach ($this->dropdown_attributes() as $key => $value) {
			$this[$key] = false;
		}
	}

	public function dropdown_selected_values()
	{
		$sel_vals = array();
		$sr = 0;
		foreach ($this->dropdown_attributes() as $key => $value) 
		{
			if($this[$key])
			{
				$sel_vals[$sr] = $key;
				$sr++;
			}
		}
		return $sel_vals;
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('policy_id',$this->policy_id);
		$criteria->compare('all_days',$this->all_days);
		$criteria->compare('sunday',$this->sunday);
		$criteria->compare('monday',$this->monday);
		$criteria->compare('teusday',$this->teusday);
		$criteria->compare('wednesday',$this->wednesday);
		$criteria->compare('thursday',$this->thursday);
		$criteria->compare('friday',$this->friday);
		$criteria->compare('saturday',$this->saturday);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PolicyDaySetting the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
