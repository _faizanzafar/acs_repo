<?php

/**
 * This is the model class for table "policy".
 *
 * The followings are the available columns in table 'policy':
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $start_date_time
 * @property string $end_date_time
 * @property integer $created_by
 * @property string $created_at
 * @property integer $updated_by
 * @property string $updated_at
 *
 * The followings are the available model relations:
 * @property SystemUser $createdBy
 * @property SystemUser $updatedBy
 * @property PolicyAssignment[] $policyAssignments
 * @property PolicyTimeSlot[] $policyTimeSlots
 * @property PolicyMonthSetting[] $policyMonthSettings
 * @property PolicyDaySetting[] $policyDaySettings
 */
class Policy extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public $selected_months;
	public $selected_week_days;
	public $customErrors=array();

	public function tableName()
	{
		return 'policy';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, start_date_time, end_date_time ', 'required'),
			array('created_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>250),
			array('description', 'length', 'max'=>500),
			array('start_date_time, end_date_time, selected_months, selected_week_days', 'safe'),

			array('created_at, created_by, updated_at, updated_by', 'unsafe'),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, description, start_date_time, end_date_time, created_by, created_at, updated_by, updated_at', 'safe', 'on'=>'search'),

			array('id, name, description, start_date_time, end_date_time, created_by, created_at, updated_by, updated_at', 'default', 'setOnEmpty' => true, 'value' => null),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'createdBy' => array(self::BELONGS_TO, 'SystemUser', 'created_by'),
			'updatedBy' => array(self::BELONGS_TO, 'SystemUser', 'updated_by'),
			'policyAssignments' => array(self::HAS_MANY, 'PolicyAssignment', 'policy_id'),
			'policyTimeSlots' => array(self::HAS_MANY, 'PolicyTimeSlot', 'policy_id'),
			'policyMonthSettings' => array(self::HAS_MANY, 'PolicyMonthSetting', 'policy_id'),
			'policyDaySettings' => array(self::HAS_MANY, 'PolicyDaySetting', 'policy_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'description' => 'Description',
			'start_date_time' => 'Start Date Time',
			'end_date_time' => 'End Date Time',
			'created_by' => 'Created By',
			'created_at' => 'Created At',
			'updated_by' => 'Updated By',
			'updated_at' => 'Updated At',
			'selected_months' => 'Months',
			'selected_week_days' => 'Week Days',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('start_date_time',$this->start_date_time,true);
		$criteria->compare('end_date_time',$this->end_date_time,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_at',$this->updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Policy the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	public function addCustomError($attribute, $error) {
		$this->customErrors[] = array($attribute, $error);
	}
	protected function beforeValidate() {
		$r = parent::beforeValidate();
		foreach ($this->customErrors as $param){
			$this->addError($param[0], $param[1]);
		}
		return $r;
	}

	public function removeTimeZone()
	{
		if($this->start_date_time != null)
			$this->start_date_time = date('d-m-Y H:i:s', strtotime($this->start_date_time));
		if($this->end_date_time != null)
			$this->end_date_time = date('d-m-Y H:i:s', strtotime($this->end_date_time));
	}

	public function beforeSave()
	{
		date_default_timezone_set('Asia/Karachi');

		$this->start_date_time = date('Y-m-d H:i:s', strtotime($this->start_date_time));
		$this->end_date_time = date('Y-m-d H:i:s', strtotime($this->end_date_time));

		return true;
	}
}
