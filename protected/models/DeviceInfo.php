<?php

/**
 * This is the model class for table "device_info".
 *
 * The followings are the available columns in table 'device_info':
 * @property integer $id
 * @property integer $device_type_id
 * @property string $unique_id
 * @property string $dsid
 * @property string $barcode
 * @property string $created_at
 * @property integer $created_by
 *
 * The followings are the available model relations:
 * @property AccessRequests[] $accessRequests
 * @property SiteDeviceInventory[] $siteDeviceInventories
 * @property DeviceType $deviceType
 * @property SystemUser $createdBy
 */
class DeviceInfo extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'device_info';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('device_type_id, created_by', 'numerical', 'integerOnly'=>true),
			array('unique_id, dsid, barcode, created_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, device_type_id, unique_id, dsid, barcode, created_at, created_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'accessRequests' => array(self::HAS_MANY, 'AccessRequests', 'dsid'),
			'siteDeviceInventories' => array(self::HAS_MANY, 'SiteDeviceInventory', 'device_info_id'),
			'deviceType' => array(self::BELONGS_TO, 'DeviceType', 'device_type_id'),
			'createdBy' => array(self::BELONGS_TO, 'SystemUser', 'created_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'device_type_id' => 'Device Type',
			'unique_id' => 'Unique',
			'dsid' => 'Dsid',
			'barcode' => 'Barcode',
			'created_at' => 'Created At',
			'created_by' => 'Created By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('device_type_id',$this->device_type_id);
		$criteria->compare('unique_id',$this->unique_id,true);
		$criteria->compare('dsid',$this->dsid,true);
		$criteria->compare('barcode',$this->barcode,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('created_by',$this->created_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DeviceInfo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
