<?php

/**
 * This is the model class for table "policy_assignment".
 *
 * The followings are the available columns in table 'policy_assignment':
 * @property integer $id
 * @property integer $policy_id
 * @property integer $system_user_id
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 *
 * The followings are the available model relations:
 * @property Policy $policy
 * @property SystemUser $systemUser
 * @property SystemUser $createdBy
 * @property SystemUser $updatedBy
 */
class PolicyAssignment extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */

	public $system_user_name;
	public $customErrors=array();

	public function tableName()
	{
		return 'policy_assignment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(

			array('system_user_name', 'required'),

			array('policy_id, system_user_id, created_by, updated_by', 'numerical', 'integerOnly'=>true),
			
			array('system_user_name', 'safe'),

			array('created_at, created_by, updated_at, updated_by, policy_id, system_user_id', 'unsafe'),
			
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, policy_id, system_user_id, created_at, created_by, updated_at, updated_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'policy' => array(self::BELONGS_TO, 'Policy', 'policy_id'),
			'systemUser' => array(self::BELONGS_TO, 'SystemUser', 'system_user_id'),
			'createdBy' => array(self::BELONGS_TO, 'SystemUser', 'created_by'),
			'updatedBy' => array(self::BELONGS_TO, 'SystemUser', 'updated_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'policy_id' => 'Policy',
			'system_user_id' => 'System User',
			'created_at' => 'Created At',
			'created_by' => 'Created By',
			'updated_at' => 'Updated At',
			'updated_by' => 'Updated By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('policy_id',$this->policy_id);
		$criteria->compare('system_user_id',$this->system_user_id);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('updated_at',$this->updated_at,true);
		$criteria->compare('updated_by',$this->updated_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PolicyAssignment the static model class
	 */

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function afterFind()
	{
		if($this->system_user_id != null)
		{
			$user = SystemUser::model()->findByPk($this->system_user_id);
			if($user != null)
			{
				$this->system_user_name = $user->full_name;
			} 
		}
	}

	public function addCustomError($attribute, $error) {
		$this->customErrors[] = array($attribute, $error);
	}
	protected function beforeValidate() {
		$r = parent::beforeValidate();

		foreach ($this->customErrors as $param){
			$this->addError($param[0], $param[1]);
		}
		return $r;
	}

	public function beforeSave()
	{
		date_default_timezone_set('Asia/Karachi');
		$date = date('Y-m-d H:i:s');

		if($this->isNewRecord)
		{
			$this->created_at = $date;
			$this->created_by = Yii::app()->user->id;
		}
		else
		{
			$this->updated_at = $date;
			$this->updated_by = Yii::app()->user->id;
		}
		return true;
	}

}
