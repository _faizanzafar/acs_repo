<?php

/**
 * This is the model class for table "work_order_status".
 *
 * The followings are the available columns in table 'work_order_status':
 * @property integer $id
 * @property integer $work_order_id
 * @property string $status
 * @property string $status_reason
 * @property integer $status_user_id
 * @property string $created_at
 * @property integer $created_by
 *
 * The followings are the available model relations:
 * @property WorkOrder $workOrder
 * @property SystemUser $statusUser
 * @property SystemUser $createdBy
 */
class WorkOrderStatus extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'work_order_status';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('work_order_id, status, status_user_id', 'required'),
			array('work_order_id, status_user_id, created_by', 'numerical', 'integerOnly'=>true),
			array('status', 'length', 'max'=>30),
			array('status_reason, created_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, work_order_id, status, status_reason, status_user_id, created_at, created_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'workOrder' => array(self::BELONGS_TO, 'WorkOrder', 'work_order_id'),
			'statusUser' => array(self::BELONGS_TO, 'SystemUser', 'status_user_id'),
			'createdBy' => array(self::BELONGS_TO, 'SystemUser', 'created_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'work_order_id' => 'Work Order',
			'status' => 'Status',
			'status_reason' => 'Status Reason',
			'status_user_id' => 'Status User',
			'created_at' => 'Created At',
			'created_by' => 'Created By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('work_order_id',$this->work_order_id);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('status_reason',$this->status_reason,true);
		$criteria->compare('status_user_id',$this->status_user_id);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('created_by',$this->created_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return WorkOrderStatus the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
