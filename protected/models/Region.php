<?php

/**
 * This is the model class for table "region".
 *
 * The followings are the available columns in table 'region':
 * @property integer $id
 * @property string $name
 * @property string $short_name
 * @property string $country
 * @property integer $customer_id
 * @property boolean $status
 * @property integer $created_by
 * @property string $created_at
 * @property integer $parent
 * @property string $type
 *
 * The followings are the available model relations:
 * @property Site[] $sites
 * @property Site[] $sites1
 * @property Site[] $sites2
 * @property SystemUser[] $systemUsers
 * @property SystemUser[] $systemUsers1
 * @property SystemUser[] $systemUsers2
 * @property Customer $customer
 * @property Region $parent0
 * @property Region[] $regions
 * @property SystemUser $createdBy
 */
class Region extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'region';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, short_name', 'unique'),
			array('name, short_name', 'required'),
			array('customer_id, created_by, parent', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>250),
			array('short_name', 'length', 'max'=>10),
			array('country', 'length', 'max'=>200),
			array('type', 'length', 'max'=>25),
			array('status', 'safe'),
			array('created_at, created_by', 'unsafe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, short_name, country, customer_id, status, created_by, created_at, parent, type', 'safe', 'on'=>'search'),

			array('id, name, short_name, country, customer_id, status, created_by, created_at, parent, type', 'default', 'setOnEmpty' => true, 'value' => null),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'sites' => array(self::HAS_MANY, 'Site', 'region_id'),
			'sites1' => array(self::HAS_MANY, 'Site', 'sub_region_id'),
			'sites2' => array(self::HAS_MANY, 'Site', 'zone_id'),
			'systemUsers' => array(self::HAS_MANY, 'SystemUser', 'region_id'),
			'systemUsers1' => array(self::HAS_MANY, 'SystemUser', 'sub_region_id'),
			'systemUsers2' => array(self::HAS_MANY, 'SystemUser', 'zone_id'),
			'customer' => array(self::BELONGS_TO, 'Customer', 'customer_id'),
			'parent0' => array(self::BELONGS_TO, 'Region', 'parent'),
			'regions' => array(self::HAS_MANY, 'Region', 'parent'),
			'createdBy' => array(self::BELONGS_TO, 'SystemUser', 'created_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'short_name' => 'Short Name',
			'country' => 'Country',
			'customer_id' => 'Customer',
			'status' => 'Status',
			'created_by' => 'Created By',
			'created_at' => 'Created At',
			'parent' => 'Parent',
			'type' => 'Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('short_name',$this->short_name,true);
		$criteria->compare('country',$this->country,true);
		$criteria->compare('customer_id',$this->customer_id);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('parent',$this->parent);
		$criteria->compare('type',$this->type,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Region the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
