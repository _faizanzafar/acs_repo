<?php

/**
 * This is the model class for table "site_device_inventory".
 *
 * The followings are the available columns in table 'site_device_inventory':
 * @property integer $id
 * @property integer $device_info_id
 * @property integer $site_id
 * @property string $device_name
 * @property string $device_description
 * @property string $created_at
 * @property integer $created_by
 *
 * The followings are the available model relations:
 * @property Site $site
 * @property DeviceInfo $deviceInfo
 * @property SystemUser $createdBy
 */
class SiteDeviceInventory extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'site_device_inventory';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('device_info_id, site_id, created_by', 'numerical', 'integerOnly'=>true),
			array('device_name, device_description, created_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, device_info_id, site_id, device_name, device_description, created_at, created_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'site' => array(self::BELONGS_TO, 'Site', 'site_id'),
			'deviceInfo' => array(self::BELONGS_TO, 'DeviceInfo', 'device_info_id'),
			'createdBy' => array(self::BELONGS_TO, 'SystemUser', 'created_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'device_info_id' => 'Device Info',
			'site_id' => 'Site',
			'device_name' => 'Device Name',
			'device_description' => 'Device Description',
			'created_at' => 'Created At',
			'created_by' => 'Created By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('device_info_id',$this->device_info_id);
		$criteria->compare('site_id',$this->site_id);
		$criteria->compare('device_name',$this->device_name,true);
		$criteria->compare('device_description',$this->device_description,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('created_by',$this->created_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SiteDeviceInventory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
