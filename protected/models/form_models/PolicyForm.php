<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class PolicyForm extends CFormModel
{
	public $isNewRecord = false;
	public $policy_id;
	public $customErrors=array();

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			// username and password are required
			array('policy_id', 'required'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'policy_id'=>'Policy',
		);
	}

	public function addCustomError($attribute, $error) {
		$this->customErrors[] = array($attribute, $error);
	}
	protected function beforeValidate() {
		$r = parent::beforeValidate();
		foreach ($this->customErrors as $param){
			$this->addError($param[0], $param[1]);
		}
		return $r;
	}
}
