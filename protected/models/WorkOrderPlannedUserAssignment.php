<?php

/**
 * This is the model class for table "work_order_planned_user_assignment".
 *
 * The followings are the available columns in table 'work_order_planned_user_assignment':
 * @property integer $id
 * @property integer $work_order_id
 * @property integer $system_user_id
 * @property boolean $active
 *
 * The followings are the available model relations:
 * @property WorkOrder $workOrder
 * @property SystemUser $systemUser
 */
class WorkOrderPlannedUserAssignment extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public $system_user_name;
	public $customErrors=array();

	public function tableName()
	{
		return 'work_order_planned_user_assignment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('system_user_name', 'required'),
			
			array('work_order_id, system_user_id', 'numerical', 'integerOnly'=>true),

			array('system_user_name, active', 'safe'),

			array('system_user_id, work_order_id', 'unsafe'),
			
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, work_order_id, system_user_id, active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'workOrder' => array(self::BELONGS_TO, 'WorkOrder', 'work_order_id'),
			'systemUser' => array(self::BELONGS_TO, 'SystemUser', 'system_user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'work_order_id' => 'Work Order',
			'system_user_id' => 'System User',
			'system_user_name' => 'User Name',
			'active' => 'Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('work_order_id',$this->work_order_id);
		$criteria->compare('system_user_id',$this->system_user_id);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return WorkOrderPlannedUserAssignment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	 public function beforeSave()
    {
    	$this->active = ($this->active == true) ? true : false;
        return true;
    }

	public function afterFind()
	{
		if($this->system_user_id != null)
		{
			$user = SystemUser::model()->findByPk($this->system_user_id);
			if($user != null)
			{
				$this->system_user_name = $user->full_name;
			} 
		}
		$this->active = ($this->active == true) ? 1 : 0;
	}

	public function addCustomError($attribute, $error) {
		$this->customErrors[] = array($attribute, $error);
	}
	protected function beforeValidate() {
		$r = parent::beforeValidate();

		foreach ($this->customErrors as $param){
			$this->addError($param[0], $param[1]);
		}
		return $r;
	}

}
