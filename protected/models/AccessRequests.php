<?php

/**
 * This is the model class for table "access_requests".
 *
 * The followings are the available columns in table 'access_requests':
 * @property integer $id
 * @property integer $system_user_id
 * @property integer $access_request_type_id
 * @property integer $dsid
 * @property integer $purpose
 * @property integer $date_time
 * @property integer $duration
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 *
 * The followings are the available model relations:
 * @property AccessRequestType $accessRequestType
 * @property DeviceInfo $ds
 * @property SystemUser $systemUser
 * @property SystemUser $createdBy
 */
class AccessRequests extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'access_requests';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('system_user_id, access_request_type_id, dsid, purpose, date_time, duration, status, created_by', 'numerical', 'integerOnly'=>true),
			array('created_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, system_user_id, access_request_type_id, dsid, purpose, date_time, duration, status, created_at, created_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'accessRequestType' => array(self::BELONGS_TO, 'AccessRequestType', 'access_request_type_id'),
			'ds' => array(self::BELONGS_TO, 'DeviceInfo', 'dsid'),
			'systemUser' => array(self::BELONGS_TO, 'SystemUser', 'system_user_id'),
			'createdBy' => array(self::BELONGS_TO, 'SystemUser', 'created_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'system_user_id' => 'System User',
			'access_request_type_id' => 'Access Request Type',
			'dsid' => 'Dsid',
			'purpose' => 'Purpose',
			'date_time' => 'Date Time',
			'duration' => 'Duration',
			'status' => 'Status',
			'created_at' => 'Created At',
			'created_by' => 'Created By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('system_user_id',$this->system_user_id);
		$criteria->compare('access_request_type_id',$this->access_request_type_id);
		$criteria->compare('dsid',$this->dsid);
		$criteria->compare('purpose',$this->purpose);
		$criteria->compare('date_time',$this->date_time);
		$criteria->compare('duration',$this->duration);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('created_by',$this->created_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AccessRequests the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
