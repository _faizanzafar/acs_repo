<?php

/**
 * This is the model class for table "system_user_access_card_log".
 *
 * The followings are the available columns in table 'system_user_access_card_log':
 * @property integer $id
 * @property integer $system_user_id
 * @property integer $access_card_id
 * @property string $message
 * @property string $created_at
 * @property integer $created_by
 *
 * The followings are the available model relations:
 * @property SystemUser $systemUser
 * @property AccessCard $accessCard
 * @property SystemUser $createdBy
 */
class SystemUserAccessCardLog extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'system_user_access_card_log';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('system_user_id, access_card_id', 'required'),
			array('system_user_id, access_card_id, created_by', 'numerical', 'integerOnly'=>true),
			array('message, created_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, system_user_id, access_card_id, message, created_at, created_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'systemUser' => array(self::BELONGS_TO, 'SystemUser', 'system_user_id'),
			'accessCard' => array(self::BELONGS_TO, 'AccessCard', 'access_card_id'),
			'createdBy' => array(self::BELONGS_TO, 'SystemUser', 'created_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'system_user_id' => 'System User',
			'access_card_id' => 'Access Card',
			'message' => 'Message',
			'created_at' => 'Created At',
			'created_by' => 'Created By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('system_user_id',$this->system_user_id);
		$criteria->compare('access_card_id',$this->access_card_id);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('created_by',$this->created_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SystemUserAccessCardLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
