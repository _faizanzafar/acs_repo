<?php

/**
 * This is the model class for table "site".
 *
 * The followings are the available columns in table 'site':
 * @property integer $site_id
 * @property string $created_at
 * @property integer $created_by
 * @property string $name
 * @property integer $region_id
 * @property integer $sub_region_id
 * @property integer $zone_id
 *
 * The followings are the available model relations:
 * @property WorkOrder[] $workOrders
 * @property SystemUser $createdBy
 * @property Region $region
 * @property Region $subRegion
 * @property Region $zone
 * @property SiteDeviceInventory[] $siteDeviceInventories
 * @property SystemUser[] $systemUsers
 */
class Site extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'site';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name','unique'),
			array('name, region_id, sub_region_id, zone_id', 'required'),
			array('created_by, region_id, sub_region_id, zone_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>50),
			
			array('created_at, created_by', 'unsafe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('site_id, created_at, created_by, name, region_id, sub_region_id, zone_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'workOrders' => array(self::HAS_MANY, 'WorkOrder', 'site_id'),
			'createdBy' => array(self::BELONGS_TO, 'SystemUser', 'created_by'),
			'region' => array(self::BELONGS_TO, 'Region', 'region_id'),
			'subRegion' => array(self::BELONGS_TO, 'Region', 'sub_region_id'),
			'zone' => array(self::BELONGS_TO, 'Region', 'zone_id'),
			'siteDeviceInventories' => array(self::HAS_MANY, 'SiteDeviceInventory', 'site_id'),
			'systemUsers' => array(self::HAS_MANY, 'SystemUser', 'site_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'site_id' => 'Site',
			'created_at' => 'Created At',
			'created_by' => 'Created By',
			'name' => 'Name',
			'region_id' => 'Region',
			'sub_region_id' => 'Sub Region',
			'zone_id' => 'Zone',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('site_id',$this->site_id);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('region_id',$this->region_id);
		$criteria->compare('sub_region_id',$this->sub_region_id);
		$criteria->compare('zone_id',$this->zone_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Site the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
