<?php

/**
 * This is the model class for table "user_mobile_imei".
 *
 * The followings are the available columns in table 'user_mobile_imei':
 * @property integer $id
 * @property integer $user_id
 * @property string $imei_no
 * @property boolean $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $expiry_time
 * @property string $sim_no
 *
 * The followings are the available model relations:
 * @property SystemUser $createdBy
 * @property SystemUser $user
 */
class UserMobileImei extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_mobile_imei';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, created_by', 'numerical', 'integerOnly'=>true),
			array('imei_no', 'length', 'max'=>25),
			array('status, created_at, expiry_time, sim_no', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, imei_no, status, created_at, created_by, expiry_time, sim_no', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'createdBy' => array(self::BELONGS_TO, 'SystemUser', 'created_by'),
			'user' => array(self::BELONGS_TO, 'SystemUser', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'imei_no' => 'Imei No',
			'status' => 'Status',
			'created_at' => 'Created At',
			'created_by' => 'Created By',
			'expiry_time' => 'Expiry Time',
			'sim_no' => 'Sim No',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('imei_no',$this->imei_no,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('expiry_time',$this->expiry_time,true);
		$criteria->compare('sim_no',$this->sim_no,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserMobileImei the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
