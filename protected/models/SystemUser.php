<?php

/**
 * This is the model class for table "system_user".
 *
 * The followings are the available columns in table 'system_user':
 * @property integer $id
 * @property string $email
 * @property string $username
 * @property string $password
 * @property string $last_login_time
 * @property string $created_at
 * @property integer $created_by
 * @property string $update_time
 * @property integer $update_user_id
 * @property integer $customer_id
 * @property boolean $login
 * @property string $notes
 * @property string $first_name
 * @property string $last_name
 * @property integer $region_id
 * @property integer $sub_region_id
 * @property string $mbu_no
 * @property integer $company_type_id
 * @property integer $company_id
 * @property string $mobile_no
 * @property string $profile_image
 * @property integer $zone_id
 * @property integer $site_id
 *
 * The followings are the available model relations:
 * @property WorkOrder[] $workOrders
 * @property WorkOrder[] $workOrders1
 * @property WorkOrder[] $workOrders2
 * @property WorkOrderPlannedUserAssignment[] $workOrderPlannedUserAssignments
 * @property SystemUserAccessCardLog[] $systemUserAccessCardLogs
 * @property SystemUserAccessCardLog[] $systemUserAccessCardLogs1
 * @property AccessLog[] $accessLogs
 * @property RequestApprover[] $requestApprovers
 * @property AlarmSubscription[] $alarmSubscriptions
 * @property SimInfo[] $simInfos
 * @property CompanyType[] $companyTypes
 * @property Company[] $companies
 * @property Policy[] $policies
 * @property Policy[] $policies1
 * @property PolicyAssignment[] $policyAssignments
 * @property PolicyAssignment[] $policyAssignments1
 * @property PolicyAssignment[] $policyAssignments2
 * @property WorkOrderStatus[] $workOrderStatuses
 * @property WorkOrderStatus[] $workOrderStatuses1
 * @property AccessRequestType[] $accessRequestTypes
 * @property AccessRequests[] $accessRequests
 * @property AccessRequests[] $accessRequests1
 * @property Site[] $sites
 * @property SiteDeviceInventory[] $siteDeviceInventories
 * @property DeviceType[] $deviceTypes
 * @property ApprovalLevel[] $approvalLevels
 * @property AccessCard[] $accessCards
 * @property UserMacAddress[] $userMacAddresses
 * @property UserMacAddress[] $userMacAddresses1
 * @property WorkOrderReason[] $workOrderReasons
 * @property WorkOrderReason[] $workOrderReasons1
 * @property SystemUserHasAccessCard[] $systemUserHasAccessCards
 * @property SystemUserHasAccessCard[] $systemUserHasAccessCards1
 * @property DeviceInfo[] $deviceInfos
 * @property UserMobileImei[] $userMobileImeis
 * @property UserMobileImei[] $userMobileImeis1
 * @property Customer $customer
 * @property Region $region
 * @property Region $subRegion
 * @property SystemUser $createdBy
 * @property SystemUser[] $systemUsers
 * @property SystemUser $updateUser
 * @property SystemUser[] $systemUsers1
 * @property CompanyType $companyType
 * @property Company $company
 * @property Region $zone
 * @property Site $site
 * @property Customer[] $customers
 * @property Region[] $regions
 * @property WorkOrderUnplannedUser[] $workOrderUnplannedUsers
 */
class SystemUser extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */

	public $image;
	public $customErrors=array();

	public function tableName()
	{
		return 'system_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(

			array('email, username', 'unique'),
			array('email, customer_id, username, password, first_name, last_name', 'required'),

			array('image', 'file', 'types'=>'jpg, gif, png', 'allowEmpty'=>true,  'safe' => false),

			array('created_by, update_user_id, customer_id, region_id, sub_region_id, company_type_id, company_id, zone_id, site_id', 'numerical', 'integerOnly'=>true),

			array('email, username, password', 'length', 'max'=>256),
			array('notes', 'length', 'max'=>500),
			array('first_name, last_name, mbu_no', 'length', 'max'=>250),
			array('login, mobile_no, zone_id, site_id', 'safe'),

			array('created_at, created_by, update_time, update_user_id , profile_image', 'unsafe'),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, email, username, password, last_login_time, created_at, created_by, , zone_id, site_id, update_user_id, customer_id, login, notes, first_name, last_name, region_id, sub_region_id, mbu_no,  company_type_id, company_id, mobile_no, profile_image', 'safe', 'on'=>'search'),

			array('id, email, username, password, last_login_time, created_at, created_by, update_time, update_user_id, customer_id, login, notes, first_name, last_name, region_id, sub_region_id, mbu_no, company_type_id, company_id, mobile_no', 'default', 'setOnEmpty' => true, 'value' => null),

		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'workOrders' => array(self::HAS_MANY, 'WorkOrder', 'requested_by'),
			'workOrders1' => array(self::HAS_MANY, 'WorkOrder', 'created_by'),
			'workOrders2' => array(self::HAS_MANY, 'WorkOrder', 'updated_by'),
			'workOrderPlannedUserAssignments' => array(self::HAS_MANY, 'WorkOrderPlannedUserAssignment', 'system_user_id'),
			'systemUserAccessCardLogs' => array(self::HAS_MANY, 'SystemUserAccessCardLog', 'system_user_id'),
			'systemUserAccessCardLogs1' => array(self::HAS_MANY, 'SystemUserAccessCardLog', 'created_by'),
			'accessLogs' => array(self::HAS_MANY, 'AccessLog', 'created_by'),
			'requestApprovers' => array(self::HAS_MANY, 'RequestApprover', 'created_by'),
			'alarmSubscriptions' => array(self::HAS_MANY, 'AlarmSubscription', 'created_by'),
			'simInfos' => array(self::HAS_MANY, 'SimInfo', 'created_by'),
			'companyTypes' => array(self::HAS_MANY, 'CompanyType', 'created_by'),
			'companies' => array(self::HAS_MANY, 'Company', 'created_by'),
			'policies' => array(self::HAS_MANY, 'Policy', 'created_by'),
			'policies1' => array(self::HAS_MANY, 'Policy', 'updated_by'),
			'policyAssignments' => array(self::HAS_MANY, 'PolicyAssignment', 'system_user_id'),
			'policyAssignments1' => array(self::HAS_MANY, 'PolicyAssignment', 'created_by'),
			'policyAssignments2' => array(self::HAS_MANY, 'PolicyAssignment', 'updated_by'),
			'workOrderStatuses' => array(self::HAS_MANY, 'WorkOrderStatus', 'status_user_id'),
			'workOrderStatuses1' => array(self::HAS_MANY, 'WorkOrderStatus', 'created_by'),
			'accessRequestTypes' => array(self::HAS_MANY, 'AccessRequestType', 'created_by'),
			'accessRequests' => array(self::HAS_MANY, 'AccessRequests', 'system_user_id'),
			'accessRequests1' => array(self::HAS_MANY, 'AccessRequests', 'created_by'),
			'sites' => array(self::HAS_MANY, 'Site', 'created_by'),
			'siteDeviceInventories' => array(self::HAS_MANY, 'SiteDeviceInventory', 'created_by'),
			'deviceTypes' => array(self::HAS_MANY, 'DeviceType', 'created_by'),
			'approvalLevels' => array(self::HAS_MANY, 'ApprovalLevel', 'created_by'),
			'accessCards' => array(self::HAS_MANY, 'AccessCard', 'created_by'),
			'userMacAddresses' => array(self::HAS_MANY, 'UserMacAddress', 'created_by'),
			'userMacAddresses1' => array(self::HAS_MANY, 'UserMacAddress', 'user_id'),
			'workOrderReasons' => array(self::HAS_MANY, 'WorkOrderReason', 'created_by'),
			'workOrderReasons1' => array(self::HAS_MANY, 'WorkOrderReason', 'updated_by'),
			'systemUserHasAccessCards' => array(self::HAS_MANY, 'SystemUserHasAccessCard', 'system_user_id'),
			'systemUserHasAccessCards1' => array(self::HAS_MANY, 'SystemUserHasAccessCard', 'created_by'),
			'deviceInfos' => array(self::HAS_MANY, 'DeviceInfo', 'created_by'),
			'userMobileImeis' => array(self::HAS_MANY, 'UserMobileImei', 'created_by'),
			'userMobileImeis1' => array(self::HAS_MANY, 'UserMobileImei', 'user_id'),
			'customer' => array(self::BELONGS_TO, 'Customer', 'customer_id'),
			'region' => array(self::BELONGS_TO, 'Region', 'region_id'),
			'subRegion' => array(self::BELONGS_TO, 'Region', 'sub_region_id'),
			'createdBy' => array(self::BELONGS_TO, 'SystemUser', 'created_by'),
			'systemUsers' => array(self::HAS_MANY, 'SystemUser', 'created_by'),
			'updateUser' => array(self::BELONGS_TO, 'SystemUser', 'update_user_id'),
			'systemUsers1' => array(self::HAS_MANY, 'SystemUser', 'update_user_id'),
			'companyType' => array(self::BELONGS_TO, 'CompanyType', 'company_type_id'),
			'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
			'zone' => array(self::BELONGS_TO, 'Region', 'zone_id'),
			'site' => array(self::BELONGS_TO, 'Site', 'site_id'),
			'customers' => array(self::HAS_MANY, 'Customer', 'created_by'),
			'regions' => array(self::HAS_MANY, 'Region', 'created_by'),
			'workOrderUnplannedUsers' => array(self::HAS_MANY, 'WorkOrderUnplannedUser', 'created_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'email' => 'Email',
			'username' => 'Username',
			'password' => 'Password',
			'last_login_time' => 'Last Login Time',
			'created_at' => 'Created At',
			'created_by' => 'Created By',
			'update_time' => 'Update Time',
			'update_user_id' => 'Update User',
			'customer_id' => 'Customer',
			'login' => 'Login',
			'notes' => 'Notes',
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'region_id' => 'Region',
			'sub_region_id' => 'Sub Region',
			'mbu_no' => 'Mbu No',
			'company_type_id' => 'Company Type',
			'company_id' => 'Company',
			'mobile_no' => 'Mobile No',
			'profile_image' => 'Profile Image',
			'zone_id' => 'Zone',
			'site_id' => 'Site',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('last_login_time',$this->last_login_time,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('update_user_id',$this->update_user_id);
		$criteria->compare('customer_id',$this->customer_id);
		$criteria->compare('login',$this->login);
		$criteria->compare('notes',$this->notes,true);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('region_id',$this->region_id);
		$criteria->compare('sub_region_id',$this->sub_region_id);
		$criteria->compare('mbu_no',$this->mbu_no,true);
		$criteria->compare('company_type_id',$this->company_type_id);
		$criteria->compare('company_id',$this->company_id);
		$criteria->compare('mobile_no',$this->mobile_no,true);
		$criteria->compare('profile_image',$this->profile_image,true);
		$criteria->compare('zone_id',$this->zone_id);
		$criteria->compare('site_id',$this->site_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SystemUser the static model class
	 */
	public function getfull_name()
	{
		return $this->first_name." ".$this->last_name;
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function addCustomError($attribute, $error) {
		$this->customErrors[] = array($attribute, $error);
	}
	protected function beforeValidate() {
		$r = parent::beforeValidate();
		foreach ($this->customErrors as $param){
			$this->addError($param[0], $param[1]);
		}
		return $r;
	}
}
