<?php

/**
 * This is the model class for table "work_order_unplanned_user".
 *
 * The followings are the available columns in table 'work_order_unplanned_user':
 * @property integer $id
 * @property integer $work_order_id
 * @property string $name
 * @property string $identification_no
 * @property string $company
 * @property string $comments
 * @property string $created_at
 * @property integer $created_by
 *
 * The followings are the available model relations:
 * @property WorkOrder $workOrder
 * @property SystemUser $createdBy
 */
class WorkOrderUnplannedUser extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'work_order_unplanned_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */

	public $customErrors=array();

	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, identification_no', 'required'),
			array('work_order_id, created_by', 'numerical', 'integerOnly'=>true),
			array('name, identification_no', 'length', 'max'=>50),
			array('comments', 'length', 'max'=>250),
			array('name, identification_no, company, comments', 'safe'),

			array('work_order_id, created_at, created_by', 'unsafe'),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, work_order_id, name, identification_no, company, comments, created_at, created_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'workOrder' => array(self::BELONGS_TO, 'WorkOrder', 'work_order_id'),
			'createdBy' => array(self::BELONGS_TO, 'SystemUser', 'created_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'work_order_id' => 'Work Order',
			'name' => 'Name',
			'identification_no' => 'Identification No',
			'company' => 'Company',
			'comments' => 'Comments',
			'created_at' => 'Created At',
			'created_by' => 'Created By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('work_order_id',$this->work_order_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('identification_no',$this->identification_no,true);
		$criteria->compare('company',$this->company,true);
		$criteria->compare('comments',$this->comments,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('created_by',$this->created_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return WorkOrderUnplannedUser the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function addCustomError($attribute, $error) {
		$this->customErrors[] = array($attribute, $error);
	}
	protected function beforeValidate() {
		$r = parent::beforeValidate();
		foreach ($this->customErrors as $param){
			$this->addError($param[0], $param[1]);
		}
		return $r;
	}
}
