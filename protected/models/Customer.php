<?php

/**
 * This is the model class for table "customer".
 *
 * The followings are the available columns in table 'customer':
 * @property integer $customer_id
 * @property string $name
 * @property integer $no_sites
 * @property string $head_office_address
 * @property string $correspondence_office_address
 * @property string $contact_person_name
 * @property string $contact_person_no
 * @property boolean $archieve
 * @property string $created_at
 * @property integer $created_by
 *
 * The followings are the available model relations:
 * @property SystemUser[] $systemUsers
 * @property SystemUser $createdBy
 * @property Region[] $regions
 */
class Customer extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'customer';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'unique'),
			array('name, no_sites', 'required'),
			array('no_sites, created_by', 'numerical', 'integerOnly'=>true),
			array('name, head_office_address, correspondence_office_address, contact_person_name, contact_person_no, archieve, created_at', 'safe'),

			array('created_at, created_by', 'unsafe'),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('customer_id, name, no_sites, head_office_address, correspondence_office_address, contact_person_name, contact_person_no, archieve, created_at, created_by', 'safe', 'on'=>'search'),
			array('customer_id, name, no_sites, head_office_address, correspondence_office_address, contact_person_name, contact_person_no, archieve, created_at, created_by', 'default', 'setOnEmpty' => true, 'value' => null),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'systemUsers' => array(self::HAS_MANY, 'SystemUser', 'customer_id'),
			'createdBy' => array(self::BELONGS_TO, 'SystemUser', 'created_by'),
			'regions' => array(self::HAS_MANY, 'Region', 'customer_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'customer_id' => 'Customer',
			'name' => 'Name',
			'no_sites' => 'Number of Sites',
			'head_office_address' => 'Head Office Address',
			'correspondence_office_address' => 'Correspondence Office Address',
			'contact_person_name' => 'Contact Person Name',
			'contact_person_no' => 'Contact Person No',
			'archieve' => 'Archieve',
			'created_at' => 'Created At',
			'created_by' => 'Created By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('customer_id',$this->customer_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('no_sites',$this->no_sites);
		$criteria->compare('head_office_address',$this->head_office_address,true);
		$criteria->compare('correspondence_office_address',$this->correspondence_office_address,true);
		$criteria->compare('contact_person_name',$this->contact_person_name,true);
		$criteria->compare('contact_person_no',$this->contact_person_no,true);
		$criteria->compare('archieve',$this->archieve);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('created_by',$this->created_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Customer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
