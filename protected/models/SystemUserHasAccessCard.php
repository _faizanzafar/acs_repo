<?php

/**
 * This is the model class for table "system_user_has_access_card".
 *
 * The followings are the available columns in table 'system_user_has_access_card':
 * @property integer $id
 * @property integer $system_user_id
 * @property integer $access_card_id
 * @property string $created_at
 * @property integer $created_by
 * @property boolean $active
 *
 * The followings are the available model relations:
 * @property AccessCard $accessCard
 * @property SystemUser $systemUser
 * @property SystemUser $createdBy
 */
class SystemUserHasAccessCard extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public $customErrors=array();
	private $statusChange = false;

	public function tableName()
	{
		return 'system_user_has_access_card';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('access_card_id', 'required'),
			array('system_user_id, access_card_id, created_by', 'numerical', 'integerOnly'=>true),
			array('created_at, active', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, system_user_id, access_card_id, created_at, created_by, active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'accessCard' => array(self::BELONGS_TO, 'AccessCard', 'access_card_id'),
			'systemUser' => array(self::BELONGS_TO, 'SystemUser', 'system_user_id'),
			'createdBy' => array(self::BELONGS_TO, 'SystemUser', 'created_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'system_user_id' => 'System User',
			'access_card_id' => 'Access Card',
			'created_at' => 'Created At',
			'created_by' => 'Created By',
			'active' => 'active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CactiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('system_user_id',$this->system_user_id);
		$criteria->compare('access_card_id',$this->access_card_id);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('active',$this->active);

		return new CactiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CactiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SystemUserHasAccessCard the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
		/*
     * Convert if null to false
     */
	protected function afterDelete()
	{
		$user_log = new SystemUserAccessCardLog;
		$user_log->system_user_id = $this->system_user_id;
		$user_log->access_card_id = $this->switchBackId();
		$user_log->message = "Card is recalled from User";
		date_default_timezone_set('Asia/Karachi');
		$date = date('Y-m-d H:i:s');
		$user_log->created_at = $date;
		$user_log->created_by = Yii::app()->user->id;
		$user_log->save();
	}
    public function beforeSave()
    {
    	$this->active = ($this->active == true) ? true : false;
    	$this->access_card_id = $this->switchBackId();

    	if( !$this->isNewRecord)
    	{
    		$curr_status = SystemUserHasAccessCard::model()->findByPk($this->id)->active;
    		if($curr_status != $this->active)
    			$this->statusChange = true;
    	}
    	
        return true;
    }
    public function afterSave()
    {
    	if( $this->isNewRecord )
    	{
    		$user_log = new SystemUserAccessCardLog;
			$user_log->system_user_id = $this->system_user_id;
			$user_log->access_card_id = $this->switchBackId();
			$user_log->message = "Card is Assigned to User";
			date_default_timezone_set('Asia/Karachi');
			$date = date('Y-m-d H:i:s');
			$user_log->created_at = $date;
			$user_log->created_by = Yii::app()->user->id;
			$user_log->save();
    	}
    	else if($this->statusChange)
    	{
    		$user_log = new SystemUserAccessCardLog;
			$user_log->system_user_id = $this->system_user_id;
			$user_log->access_card_id = $this->switchBackId();

			if($this->active)
    			$user_log->message = "Card is Activated";
    		else
    			$user_log->message = "Card is Deactivated";

			
			date_default_timezone_set('Asia/Karachi');
			$date = date('Y-m-d H:i:s');
			$user_log->created_at = $date;
			$user_log->created_by = Yii::app()->user->id;
			$user_log->save();
			
    		$this->statusChange = false;
    	}
    }

   public function afterFind()
   {
       $this->active = ($this->active == true) ? 1 : 0;
       $this->access_card_id = $this->switchToString();
   }

    public function addCustomError($attribute, $error) {
		$this->customErrors[] = array($attribute, $error);
	}
	protected function beforeValidate() {
		$r = parent::beforeValidate();

		foreach ($this->customErrors as $param){
			$this->addError($param[0], $param[1]);
		}
		return $r;
	}

	public function getaccess_card_id()
	{
		return $this->switchToString();
	}

	public function switchToString()
	{
		if($this->access_card_id != null)
		{
			$acc_card = AccessCard::model()->findByPk(intval($this->access_card_id));
			if($acc_card != null)
			 	return $acc_card->qr_code;
			return $this->access_card_id;
		}
		return null;
	}
	public function switchBackId()
	{
		if($this->access_card_id != null)
		{
			$acc_card = AccessCard::model()->findByAttributes( array('qr_code'=> ((string)$this->access_card_id) ));
			if($acc_card != null)
			 	return $acc_card->id;
			return $this->access_card_id;
		}
		return null;
	}
}
