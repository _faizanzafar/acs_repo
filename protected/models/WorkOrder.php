<?php

/**
 * This is the model class for table "work_order".
 *
 * The followings are the available columns in table 'work_order':
 * @property integer $id
 * @property string $unique_id
 * @property integer $reason_id
 * @property integer $site_id
 * @property integer $requested_by
 * @property boolean $planned
 * @property string $work_order_date
 * @property string $start_time
 * @property string $end_time
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 *
 * The followings are the available model relations:
 * @property WorkOrderReason $reason
 * @property Site $site
 * @property SystemUser $requestedBy
 * @property SystemUser $createdBy
 * @property SystemUser $updatedBy
 * @property WorkOrderPlannedUserAssignment[] $workOrderPlannedUserAssignments
 * @property WorkOrderStatus[] $workOrderStatuses
 * @property WorkOrderUnplannedUser[] $workOrderUnplannedUsers
 */
class WorkOrder extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */

	public $customErrors=array();

	public function tableName()
	{
		return 'work_order';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('reason_id, site_id, work_order_date', 'required'),
			array('reason_id, site_id, requested_by, created_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('unique_id', 'length', 'max'=>25),
			array('start_time, end_time', 'safe'),

			array('planned, created_at, created_by, updated_at, updated_by', 'unsafe'),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, unique_id, reason_id, site_id, requested_by, planned, work_order_date, start_time, end_time, created_at, created_by, updated_at, updated_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'reason' => array(self::BELONGS_TO, 'WorkOrderReason', 'reason_id'),
			'site' => array(self::BELONGS_TO, 'Site', 'site_id'),
			'requestedBy' => array(self::BELONGS_TO, 'SystemUser', 'requested_by'),
			'createdBy' => array(self::BELONGS_TO, 'SystemUser', 'created_by'),
			'updatedBy' => array(self::BELONGS_TO, 'SystemUser', 'updated_by'),
			'workOrderPlannedUserAssignments' => array(self::HAS_MANY, 'WorkOrderPlannedUserAssignment', 'work_order_id'),
			'workOrderStatuses' => array(self::HAS_MANY, 'WorkOrderStatus', 'work_order_id'),
			'workOrderUnplannedUsers' => array(self::HAS_MANY, 'WorkOrderUnplannedUser', 'work_order_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'unique_id' => 'Unique',
			'reason_id' => 'Reason',
			'site_id' => 'Site',
			'requested_by' => 'Requested By',
			'planned' => 'Planned',
			'work_order_date' => 'Work Order Date',
			'start_time' => 'Start Time',
			'end_time' => 'End Time',
			'created_at' => 'Created At',
			'created_by' => 'Created By',
			'updated_at' => 'Updated At',
			'updated_by' => 'Updated By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('unique_id',$this->unique_id,true);
		$criteria->compare('reason_id',$this->reason_id);
		$criteria->compare('site_id',$this->site_id);
		$criteria->compare('requested_by',$this->requested_by);
		$criteria->compare('planned',$this->planned);
		$criteria->compare('work_order_date',$this->work_order_date,true);
		$criteria->compare('start_time',$this->start_time,true);
		$criteria->compare('end_time',$this->end_time,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('updated_at',$this->updated_at,true);
		$criteria->compare('updated_by',$this->updated_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return WorkOrder the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave()
	{
		$this->work_order_date = date('Y-m-d', strtotime($this->work_order_date));
		return true;
	}

	public function afterFind()
	{
		if($this->start_time == null || $this->start_time == "")
			$this->start_time = "00:00:00";
		if($this->end_time == null || $this->end_time == "")
			$this->end_time = "23:59:59";

		$this->work_order_date = date('d-m-Y', strtotime($this->work_order_date));
	}

	public function addCustomError($attribute, $error) {
		$this->customErrors[] = array($attribute, $error);
	}
	protected function beforeValidate() {
		$r = parent::beforeValidate();
		foreach ($this->customErrors as $param){
			$this->addError($param[0], $param[1]);
		}
		return $r;
	}
}
