<?php

/**
 * This is the model class for table "access_card".
 *
 * The followings are the available columns in table 'access_card':
 * @property integer $id
 * @property string $created_at
 * @property integer $created_by
 * @property string $barcode
 * @property string $qr_code
 *
 * The followings are the available model relations:
 * @property SystemUserAccessCardLog[] $systemUserAccessCardLogs
 * @property SystemUser $createdBy
 * @property SystemUserHasAccessCard[] $systemUserHasAccessCards
 */
class AccessCard extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'access_card';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('barcode, qr_code', 'unique'),
			array('barcode, qr_code', 'required'),
			array('created_by', 'numerical', 'integerOnly'=>true),
			array('barcode, qr_code', 'length', 'max'=>25),

			array('created_at, created_by', 'unsafe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, created_at, created_by, barcode, qr_code', 'safe', 'on'=>'search'),
			array('id, created_at, created_by, barcode, qr_code', 'default', 'setOnEmpty' => true, 'value' => null),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'systemUserAccessCardLogs' => array(self::HAS_MANY, 'SystemUserAccessCardLog', 'access_card_id'),
			'createdBy' => array(self::BELONGS_TO, 'SystemUser', 'created_by'),
			'systemUserHasAccessCards' => array(self::HAS_MANY, 'SystemUserHasAccessCard', 'access_card_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'created_at' => 'Created At',
			'created_by' => 'Created By',
			'barcode' => 'Barcode',
			'qr_code' => 'Qr Code',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('barcode',$this->barcode,true);
		$criteria->compare('qr_code',$this->qr_code,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AccessCard the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
