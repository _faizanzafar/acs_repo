<?php
class CClass 
{
	public function time_overlapes($time_slots, $start_time, $end_time)
	{
		if(is_array($time_slots))
		foreach ($time_slots as $key => $value) 
		{
			if( ( $start_time > $value["start_time"] && $start_time < $value["end_time"]) ||
				( $value["start_time"] > $start_time && $value["start_time"] < $end_time) )
			{
				return true;
			}
		}
		else
			return false;

		return false;
	}

	public function test_time($timeStr)
	{
	    $dateObj = DateTime::createFromFormat('d.m.Y H:i:s', "10.10.2010 " . $timeStr);

	    if ( $dateObj !== false && $dateObj && $dateObj->format('G') == intval($timeStr) )
	    {
	        return true;
	    }
	    else
	    {
	      return false;
	    }
	}

	public function validateDate($date, $format = 'd-m-Y H:i:s')
	{
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}
}